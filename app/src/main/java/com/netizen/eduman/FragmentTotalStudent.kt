package com.netizen.eduman


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.TotalStudentAdapter
import com.netizen.eduman.FragmentSAttendanceSelection.Companion.localPeriodID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.TotalStudent
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*

class FragmentTotalStudent : Fragment() {
    private var result_student_adt_id_search: EditText? = null

    internal var totalStudentArrayList = ArrayList<TotalStudent>()
    private var recyclerView: RecyclerView? = null
    private var adapter: TotalStudentAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private val total_std_section_single_view: TextView? = null
    private var v: View? = null

    private var status: String? = null
    private var header: String? = null
    private var section: String? = null
    private var section_id: String? = null
    private var section_date: String? = null

    private var header_txt: TextView? = null
    private var section_txt: TextView? = null
    private var txt_back: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.total_student_recyler_list_pop_up_st_smry, container, false)
        initializeViews()
        recyclerViewInit()
        try {

            if (savedInstanceState == null) {
                if (arguments != null) {
                    status = arguments?.getString("status")
                    header = arguments?.getString("header")
                    section = arguments?.getString("section")
                    section_id = arguments?.getString("section_id")
                    section_date = arguments?.getString("section_date")

                    val mainFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    val parsedDate = mainFormat.parse(section_date)
                    val formatGet = SimpleDateFormat("MM/dd/yyyy", Locale.US)
                    section_date = formatGet.format(parsedDate)

                    header_txt?.text = header
                    section_txt?.text = "Section: $section"

                    GetTotalStudentAttendanceSummary()
                }
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }

        return v

    }

    private fun initializeViews() {

        result_student_adt_id_search = v?.findViewById<View>(R.id.et_student_atd_smry_search) as EditText

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        header_txt = v?.findViewById<View>(R.id.attendance_summary_header_popup) as TextView
        section_txt = v?.findViewById<View>(R.id.total_atdnce_smry_text_popup_section) as TextView

        result_student_adt_id_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentAttendanceSummary()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun recyclerViewInit() {
        // Initialize item list
        totalStudentArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.total_st_atd_smry_rcyler_list_id_popup) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = TotalStudentAdapter(activity!!, totalStudentArrayList)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    fun GetTotalStudentAttendanceSummary() {
        // val hitURL = "http://192.168.31.14:8080/attendance/report/status-and-sec-wise/std-details/by/period-id/and/date?attendanceDate=04/21/2019&periodId=926552301&classConfigId=117556&attendanceStatus=$status&access_token=6b1870b4-23af-416d-af16-65b0f1854019"

        val hitURL =
            AppController.BaseUrl + "attendance/report/status-and-sec-wise/std-details/by/period-id/and/date?attendanceDate=" + section_date + "&periodId=" + localPeriodID + "&classConfigId=" + section_id + "&attendanceStatus=$status&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data..") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val totalStudent = TotalStudent()

                        totalStudent.setStd_id(c.getString("studentId"))
                        totalStudent.setStd_roll(c.getString("studentRoll"))
                        totalStudent.setStd_name(c.getString("studentName"))
                        totalStudent.setStd_gender(c.getString("gender"))
                        totalStudent.setStd_mobile_no(c.getString("mobileNo"))
                        totalStudent.setStd_status(c.getString("attendanceStatus"))

                        // adding item to ITEM list
                        totalStudentArrayList.add(totalStudent)

                    }
                    if (totalStudentArrayList.isEmpty()) {
                        Log.d("Attendance: summary", "Attendance summary data found")
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.fail,
                                "Failure",
                                "Sorry! no data found"
                            )
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {
        private val TAG = "FragmentTotalStudent"
    }
}
