package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.model.MarkDistribution

class MarkDistributionAdapter(private val mContext: Context, private val markDistributionList: List<MarkDistribution>) :
    RecyclerView.Adapter<MarkDistributionAdapter.MarkDistributionViewHolder>() {

    inner class MarkDistributionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var examCode: TextView
        var totalMark: TextView
        var passMark: TextView
        var markAcceptance: TextView

        init {
            examCode = view.findViewById<View>(R.id.exam_code_id) as TextView
            totalMark = view.findViewById<View>(R.id.total_mark) as TextView
            passMark = view.findViewById<View>(R.id.pass_mark) as TextView
            markAcceptance = view.findViewById<View>(R.id.acceptance) as TextView
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MarkDistributionAdapter.MarkDistributionViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.mark_distribution_row, parent, false)
        return MarkDistributionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MarkDistributionAdapter.MarkDistributionViewHolder, position: Int) {
        val markDistribution = markDistributionList[position]

        holder.examCode.text = markDistribution.exameCode
        holder.totalMark.text = markDistribution.totalMark
        holder.passMark.text = markDistribution.passMark
        holder.markAcceptance.text = markDistribution.acceptance

    }

    override fun getItemCount(): Int {
        return markDistributionList.size
    }
}
