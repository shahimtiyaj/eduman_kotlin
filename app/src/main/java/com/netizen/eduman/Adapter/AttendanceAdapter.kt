package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.Attendance
import com.netizen.eduman.model.ResultSummary
import java.util.ArrayList


class AttendanceAdapter(private val mContext: Context, private val attendancesList: List<Attendance>) :
    RecyclerView.Adapter<AttendanceAdapter.AttendanceViewHolder>(), Filterable {

    private var filteredatdList: List<Attendance>? = null

    inner class AttendanceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var student_id: TextView
        var student_roll: TextView
        var student_name: TextView
        var student_gender: TextView
        var checkBox: CheckBox

        init {
            student_id = view.findViewById<View>(R.id.attendance_std_id) as TextView
            student_roll = view.findViewById<View>(R.id.attendance_std_roll) as TextView
            student_name = view.findViewById<View>(R.id.attendance_std_name) as TextView
            student_gender = view.findViewById<View>(R.id.attendance_std_gender) as TextView
            checkBox = view.findViewById<View>(R.id.checkbox) as CheckBox
        }
    }
    init {
        this.filteredatdList = attendancesList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttendanceViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.take_attendance_item_row, parent, false)
        // Return a new holder instance
        return AttendanceViewHolder(itemView)
    }


    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: AttendanceViewHolder, position: Int) {
        // Get the item model based on position
        val attendance = filteredatdList?.get(position)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val Cflag = attendance?.getStudent_absent()
        val student_identification = attendance?.getstudent_identification()
        // Set item views based on our views and data model
        holder.student_id.text = attendance?.student_id
        holder.student_roll.text = attendance?.student_roll_no
        holder.student_name.text = attendance?.student_name
        holder.student_gender.text = attendance?.student_gender
        //holder.checkBox.setText(Cflag);

        if (Cflag == 0) {
            holder.checkBox.isChecked = false
        } else {
            holder.checkBox.isChecked = true

        }

        holder.checkBox.setOnClickListener {
            if (Cflag == 0) {
                student_identification?.let { it1 -> dao?.UpdateSingleCategoryFlag(1, it1) }
            } else {
                student_identification?.let { it1 -> dao?.UpdateSingleCategoryFlag(0, it1) }
            }
        }

       // dao?.close()

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredatdList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredatdList = attendancesList
                } else {
                    val filteredList = ArrayList<Attendance>()
                    for (row in attendancesList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.student_id!!.toLowerCase().contains(charString.toLowerCase()) || row.student_name!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredatdList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredatdList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredatdList = filterResults.values as ArrayList<Attendance>
                notifyDataSetChanged()
            }
        }
    }
}
