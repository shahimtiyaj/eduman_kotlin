package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.InputMark
import com.netizen.eduman.model.MarkDistribution

class UpdateMarkAdapter(private val mContext: Context, private val inputMarksList: List<InputMark>) :
    RecyclerView.Adapter<UpdateMarkAdapter.InputMarkViewHolder>() {
    private val markDistributionList: List<MarkDistribution>? = null


    inner class InputMarkViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var input_mark_st_id: TextView
        var input_mark_st_roll: TextView
        var input_mark_st_name: TextView
        var input_mark_exam_name1: TextView
        var input_mark_exam_name2: TextView
        var input_mark_exam_name3: TextView
        var input_exam_mark1: EditText
        var input_exam_mark2: EditText
        var input_exam_mark3: EditText

        internal var exam: String

        init {

            input_mark_st_id = view.findViewById<View>(R.id.input_mark_std_id) as TextView
            input_mark_st_roll = view.findViewById<View>(R.id.input_mark_std_roll) as TextView
            input_mark_st_name = view.findViewById<View>(R.id.input_mark_std_name) as TextView
            input_mark_exam_name1 = view.findViewById<View>(R.id.input_mark_examCodeTitle_1) as TextView
            input_mark_exam_name2 = view.findViewById<View>(R.id.input_mark_examCodeTitle_2) as TextView
            input_mark_exam_name3 = view.findViewById<View>(R.id.input_mark_examCodeTitle_3) as TextView

            input_exam_mark1 = view.findViewById<View>(R.id.input_et_mark_std_examCode1) as EditText
            input_exam_mark2 = view.findViewById<View>(R.id.input_et_mark_std_examCode2) as EditText
            input_exam_mark3 = view.findViewById<View>(R.id.input_et_mark_std_examCode3) as EditText


            exam = input_exam_mark1.text.toString()

            input_exam_mark1.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable) {}
            })


            input_exam_mark2.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode2 = input_exam_mark2.text.toString()

                    val shortCode2 = inputMarksList[adapterPosition].shortCode2

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id
                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode2(shortCode2!!, idNo!!)
                    dao?.close()
                }
            })


            input_exam_mark3.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode3 = input_exam_mark3.text.toString()

                    val shortCode3 = inputMarksList[adapterPosition].shortCode3

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode3(shortCode3!!, idNo!!)
                    dao?.close()
                }
            })

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpdateMarkAdapter.InputMarkViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.update_mark_item_row, parent, false)
        return InputMarkViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UpdateMarkAdapter.InputMarkViewHolder, position: Int) {
        val inputMark = inputMarksList[position]

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        var defaultId: String? = null
        var examName: String

        val inputMarks = dao?.getALLMarkDistrubutionScale()

        for (mark in inputMarks!!) {

            defaultId = mark.defaultId
            examName = dao.GetExamCode2(defaultId!!)

            if (defaultId == "1") {
                holder.input_mark_exam_name1.visibility = View.VISIBLE
                holder.input_exam_mark1.visibility = View.VISIBLE
                holder.input_mark_exam_name1.text = examName
                holder.input_exam_mark1.setText(inputMark.shortCode1)
            }

            if (defaultId == "2") {
                holder.input_mark_exam_name2.visibility = View.VISIBLE
                holder.input_exam_mark2.visibility = View.VISIBLE
                holder.input_mark_exam_name2.text = examName
                holder.input_exam_mark2.setText(inputMark.shortCode2)

            } else if (defaultId == "3") {
                holder.input_mark_exam_name3.visibility = View.VISIBLE
                holder.input_exam_mark3.visibility = View.VISIBLE
                holder.input_mark_exam_name3.text = examName
                holder.input_exam_mark3.setText(inputMark.shortCode3)
            }
        }

        holder.input_mark_st_id.text = inputMark.in_mark_st_id
        holder.input_mark_st_roll.text = inputMark.in_mark_st_roll
        holder.input_mark_st_name.text = inputMark.in_mark_st_name
        dao.close()
    }

    override fun getItemCount(): Int {
        return inputMarksList.size
    }

}
