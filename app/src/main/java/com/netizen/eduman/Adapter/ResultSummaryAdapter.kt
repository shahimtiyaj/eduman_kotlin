package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.model.ResultSummary
import java.util.ArrayList

class ResultSummaryAdapter(private val mContext: Context, private val resultSummaryList: List<ResultSummary>) :
    RecyclerView.Adapter<ResultSummaryAdapter.ResultSummaryViewHolder>(),
    Filterable {
    private var filteredresultSummaryList: List<ResultSummary>? = null


    inner class ResultSummaryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var result_std_id: TextView
        var result_std_roll: TextView
        var result_std_name: TextView
        var result_std_total_marks: TextView
        var result_std_grade: TextView
        var result_std_gpa: TextView
        var result_std_status: TextView
        var result_std_section_merit: TextView
        var result_std_no_of_fail: TextView

        init {
            result_std_id = view.findViewById<View>(R.id.result_std_id) as TextView
            result_std_roll = view.findViewById<View>(R.id.result_std_roll_no) as TextView
            result_std_name = view.findViewById<View>(R.id.result_std_name) as TextView
            result_std_total_marks = view.findViewById<View>(R.id.result_std_total_marks) as TextView
            result_std_grade = view.findViewById<View>(R.id.result_std_grade) as TextView
            result_std_gpa = view.findViewById<View>(R.id.result_std_gpa) as TextView
            result_std_status = view.findViewById<View>(R.id.result_std_status) as TextView
            result_std_section_merit = view.findViewById<View>(R.id.result_std_section_merit) as TextView
            result_std_no_of_fail = view.findViewById<View>(R.id.result_std_no_of_fail_subject) as TextView
        }
    }

    init {
        this.filteredresultSummaryList = resultSummaryList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultSummaryViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.result_summary_item_row, parent, false)
        return ResultSummaryViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ResultSummaryViewHolder, position: Int) {

        val hr = filteredresultSummaryList!![position]

/*        val animation = AnimationUtils.loadAnimation(mContext, R.anim.translate)
        animation.startOffset = (30 * position).toLong()//Provide delay here
        holder.itemView.startAnimation(animation)*/

        holder.result_std_id.text = hr.result_std_id
        holder.result_std_roll.text = hr.result_std_roll
        holder.result_std_name.text = hr.result_std_name
        holder.result_std_total_marks.text = hr.result_std_total_mark
        holder.result_std_grade.text = hr.result_std_grade
        holder.result_std_gpa.text = hr.result_std_gpa
        holder.result_std_status.text = hr.result_std_status
        holder.result_std_section_merit.text = hr.result_std_sec_merit
        holder.result_std_no_of_fail.text = hr.result_std_no_failed
    }

    override fun getItemCount(): Int {
        return filteredresultSummaryList!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredresultSummaryList = resultSummaryList
                } else {
                    val filteredList = ArrayList<ResultSummary>()
                    for (row in resultSummaryList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.result_std_id!!.toLowerCase().contains(charString.toLowerCase()) || row.result_std_name!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredresultSummaryList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredresultSummaryList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredresultSummaryList = filterResults.values as ArrayList<ResultSummary>
                notifyDataSetChanged()
            }
        }
    }

}

