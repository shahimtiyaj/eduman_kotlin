package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Student
import java.util.ArrayList

class StudentListAdapter(val studentList: ArrayList<Student>?, val mContext: Context) :
    RecyclerView.Adapter<StudentListAdapter.StudentViewHolder>(), Filterable {

    private var filteredresultstudentList: List<Student>? = null


    //private val mContext: Context? = null
    // private val studentlist: List<Student>?= null

    inner class StudentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var st_id: TextView
        var st_roll: TextView
        var st_name: TextView
        var group: TextView
        var category: TextView
        var gender: TextView
        var religion: TextView
        var fname: TextView
        var mname: TextView
        var mobile: TextView
        var image: ImageView

        init {
            st_id = view.findViewById<View>(R.id.student_id) as TextView
            st_roll = view.findViewById<View>(R.id.student_roll) as TextView
            st_name = view.findViewById<View>(R.id.student_name) as TextView
            group = view.findViewById<View>(R.id.student_group) as TextView
            category = view.findViewById<View>(R.id.student_category) as TextView
            gender = view.findViewById<View>(R.id.student_gender) as TextView
            religion = view.findViewById<View>(R.id.student_religion) as TextView
            fname = view.findViewById<View>(R.id.student_father_name) as TextView
            mname = view.findViewById<View>(R.id.student_mother_name) as TextView
            mobile = view.findViewById<View>(R.id.student_mobile) as TextView

            image = view.findViewById<View>(R.id.student_image_id) as ImageView
        }
    }

    init {
        this.filteredresultstudentList = studentList
    }
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.student_list_item, parent, false)
        // Return a new holder instance
        return StudentViewHolder(itemView)
    }


    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        // Get the item model based on position
        val student = filteredresultstudentList?.get(position)

/*        val animation = AnimationUtils.loadAnimation(mContext, R.anim.translate)
        animation.startOffset = (30 * position).toLong()//Provide delay here
        holder.itemView.startAnimation(animation)*/

        // Set item views based on our views and data model
        holder.st_id.text = student?.getstudent_id()
        holder.st_roll.text = student?.getstudent_roll()
        holder.st_name.text = student?.getstudent_name()
        holder.group.text = student?.getgroup()
        holder.category.text = student?.getcategory()
        holder.gender.text = student?.getgender()
        holder.religion.text = student?.getreligion()
        holder.fname.text = student?.getfather_name()
        holder.mname.text = student?.getmother_name()
        holder.mobile.text = student?.getmobile_no()
 /*
        Glide.with(AppController.instance)
            .load(
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUTExIWEhQVFxYXFhEYFRUVFRYYFxUZGCAdFRsdHyggJBslHR0WIT0hJSkrLi4uHiAzODMtOSgtLisBCgoKDg0OGxAQGy0lICUtLy4rKy0tLS03NS0vLS8tLS0tLS0vKy0vLTcuLS0tLS4tLS8tLS0vNy0rLSsvNy0tLf/AABEIAIAAgAMBEQACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABwQFBgIDAf/EAD8QAAEDAgIGBgcFBwUAAAAAAAEAAgMEEQUhBhIxQVFhBzRxgZGxEyIycqHB4VJzg5LRFUJDU2KC8RQzorLC/8QAGwEAAQUBAQAAAAAAAAAAAAAAAAECAwQFBgf/xAAzEQACAQIDBAgGAgMBAAAAAAAAAQIDEQQhMQUSQWETMlFxgZGx4QYUIsHR8EKhFTNTUv/aAAwDAQACEQMRAD8AeKABAAgAQBGqq6OPabn7IzKzsZtTDYXKcrvsWb9vEmp0Jz0RVz408+yA0cTmVzmI+JK0sqMVFc83+PUtwwcV1ncyWM6ewREtMzpHDa2PMd5yalo4Xa2MW9Kbin2u39L8BKpQp5JXMzP0kuPswd7pCfhqq6vhlv8A2Vm/D8si+d7InVP0kke1Bbm2T5EfNJL4ZlH/AF1n5fh/YVY1PWJqcF03hmIayZzHnZHJkT2XuD2AqhXobVwK3t5uK4p7y8n+CWMqFXK2fkaiDGnj2gHcxkU7D/ElaOVaKkuWT/HoJPBxfVdi0pa6OTYbH7JyK6PB7Uw2KyhKz7Hk/fwKlShOGqJK0SEEACABAAgAQBy94AuTYDemVKkacXObslxFSbdkUddixdkz1R9ref0XG7R2/Oq3DD5R7eL/AB6mjRwqjnPUqZZA0FzjYAEknYAN5XOpSnKyzbLd0kK/SrS19QTHESyEZcHSc3cuXjy73ZOxIYZKpVzn/S7ufPyMqviXPKOhllvlUEACABAGy0S0wdGWwznWjOTZCfWZ2ne3yXM7X2FGqnWoK0uK7ff1LuHxTj9M9BktO8eK4jNM0i0ocWLcn+sPtbx+q6PZ2350moYjOPbxX59SpWwqlnDUvGPBFwbg712VOpGpFTg7p8TOaadmdJ4gIAEAcveACSbAbSmVKkacXObslqKk27IzeI1xkPBo2D5lef7U2pPGTssoLRfd/uRq0KCprmQ1klgwnSRjJAbTMO2zpOzc35+C634awCbeJmuUfu/t5mfjav8ABeIv12JnggAQAIA6LSLZbdnPdkgDlADL6OsZMkZgebuiF2nizZ8Dl2ELhviPAKlVVeCylr3+/wCTTwdXejuvgbBc0XSZh1cYzxado+YWtsvak8HOzzg9V91+5levQVRczSMeCAQbg7CvQKdSNSKnB3T0Mppp2Z0niAgChxqs1jqDYNvM/RcVt/aLqz+Xg/pWvN+3qaWFo7q33qVa5suAgBLaRVRlqZnne9wHY06o+AC9R2dRVHC04LsXm83/AGYdaW9NsrVdIwQAIAs8AwWWrlEcYyy13/usbxP6b1HUqKCux8IObsjfabaLNNLH6BmdOLao2uZtPab+t4qlQr/W97iWa1L6cuArlolMvNC6ox1kVtjjqHmHZedj3LK21RVXBTvwV/L2J8NLdqIb682NkEAWmC1mqdQ7Ds5H6rpNgbRdKfy839L05P39SniqO8t9al8u1M0jYjUejYTv2DtKztqYz5XDSmtXku9/tyahT35pGWXnDdzXBIKCAEjHQyyzmJjdeQufZtwLltyduWwFesU5xVJS4WRg7rcrIlSaMVzdtNL3NLvJKq1PtF6KfYekGiVe/ZTPHvWZ/wBiEjr01xFVKb4Giwno3lJBqJAxu9jPWd2XOQ+KgnjF/FEscM/5DAwvDIadno4WBjdp3kni47SVSnOU3dlqMVFWRLTRxlcc0Epahxe0mB5zJaAWk8S3j2EKzTxM4qzzIJ0Iyz0F5S0JhxBkIdrGOoY3Wta9njOylxs08HUk/wDw/QrQjaqlzHAvLzbBAAlTsIanDqj0jAd+w9oXo+y8Z81hozeqyfev25kV6e5Noq8fmu4N4C/efouc+JMRvVo0VpFX8X7epbwcLRcu0qlzRdBAAgBf4bRehxtrdznSPb2PhefgbjuXomAxHT7OjLikk/B29zKcN3EWGamloEACABAAgAQAscOovSYxMd0ckjz2g2HxIPco9s4jotn24ysvu/6K1GG9XfIYK4E1AQAIAtcAms4t4i/ePoul+G8Ru1pUXpJX8V7ehSxkLxUuwhV79aR55keGSyNpVekxdSXNryy+xYoxtTSI6okoIAEAV1dhodPBO324nEHmx4LSO69/Fbexsd0MnRl1Z+vDz08ivWp7zUlqi+XTjAQAIAEACABAFBg2GiIzSH25pXvceDS46re4fElcntfHfM1Uo9WKsvu/3gSUae4m3qyyWSTggAQBIoH6sjDzA8cle2bV6PF05c0vPL7kVaN6bR4vNye1VJy3pN8yRZI5TBQQAIAEqdswJcbrhdtgsUsRSUuPHvK0lZnStjQQAIAEAcSvsFn7SxSoUXbV5L8j4RuyKuNLAIAEACAOmGxHanwluyT5iPNA8WJHNE1uya5gndHKYKCAPKoqY2C73tYP6iB5qWnRqVXaEW+5XEbS1If7dpf5zfirX+Lxf/NjOlh2n1mkFKP4zfirWFwuPw096NN812iSnB8S0ocQimF43hwBsbcV00JOUU5RafYyHuJKeAIAi12IxQgGR4bc2F+KiqzlGLcIuT7ELlxKx2kFKf4zfiuYxGDx9ee/ODJVOC4nz9u0v85vxUH+Lxf/ADYvSw7SZT1Mbxdj2vH9JB8lVqUalJ2nFrvVh6aeh6qIUEAdMFyBzT4LeklzEbsj2r2asjxzJ8c1b2lS6PF1I82/PP7kdGV6aZHVElIWMV4gidIczsaOLjsVzA4R4qsqa8e4ZOW6ri3qaiSVxc9xc47z8uS9Ao0KdGG5TVkUG23dnyKlkcQ1rS5xNg0C5J5BSiGlw3QGsksX6sLf6jd1uQF/iQnqmyN1EhoUWDQRQtga2zGjb+8TvcTxKWdKM47rI41JRlvIqa2lkiO0lu527/Kyq1KdJ8jRpVI1FzPtDSPlO0hu8/pzS0aM6r5CVasaa5llieCQTwGBzfVOYd+8125w5/4WrGnGMd1Ge6knLeYr8R0ErYrlrWzN4sOf5TY+F01wZKqiZnZKaRpLXNLSMi0ixB5gpg8+09RJE4OY4tcN4+fJRVqFOtDcqK6FTad0MjB68TxNkGR2OHBw2rz/AB2EeFrOm/DuL8JbyuTVTHkigZrSMHMHwzV7ZtLpMXTjzT8s/sRVpWptk3H4bODuIt3j6LX+JMPu1o1lpJW8V7ehXwc7xcewqlzRdIeKYC+sa1jXBjQ8Oc8i9hqkZDec10vw3QlKtKpbK1r87oqYqoopIrtIdE6ekptdhc+TWaNdx3G97AZea7CUEkUoTbZSaNdag+8b5pkdSSfVY31ZKh7MOSaKZ/TjHW0tOdhkku2Npz7XHkPOymoYdVnaWnEa5uGa1PXQ3G2VdO1ws17LNkYNx4jkdvjwRWodDLdWnAVT3s2XchyUKFPFOEFDpR1ub3yq0tS3DqoudHdE6erptd5cyTWcNZp3AC1wct/JPjFNEc5uLLLC8BfRtcxzg9peXNeBbLVAzG45Lj/iShONWNS2VrX53bLuFqKSaJi5otlrgEN3F3AW7z9F0vw3h96tKs9Iq3i/b1KWMnaKj2lpiNP6RhG/aO0Lo9qYP5rDSgtVmu9ftipQqbk0zLLzhqxrmgoY9VjRyv4r0jZNDocJCPFq78czHry3qjZQ9IPVP72fNXqmg2l1jC6Ndag+8b5qKOpPPqsb6slQ7jKRgKTpKgnFWXyG7HAeiO4NG1vaDn33WrhHHo7LxIKl7np0Yxz/AOqLmG0YaRLwIOwe9fPuKMY47lnrwFp3uNeQrKRMcJQFDpR1ub3yq0tS3Dqo3HR71X8R3kFLT0IavWL+uj1mOHK/hmqO1qHTYSceKV14Zi0JbtRMz683SubBqcOp/RsA37T2lej7LwfyuGjB6vN97/bGRXqb82yStEhKTFKD1w5uxxAPIlchtbZDeJjUpr6ZtJ8m+Pc/U0KGI+hp6osAuwSSVkZxmukHqn97PmmVNCSl1jC6Ndag+8b5qKOpPPqsb6slQEAL3pYn/wBhnvuP/ED/ANK/gVqyKqddE8+U8fNjh36wPkEY5aMKQwFQJQQAodKOtze+VWlqW4dVG46Peq/iO8gpaehDV6xpintXVmRFfheH+uXOGTSQOZC5DZOyGsTKpUX0wbS5tce5epoV8R9CS1ZdrrigCAPjhdI1fJgR3tt+qE+DAzHSD1T+9nzSVNB9LrGF0a61B943zUUdSefVY31ZKgIAVPSdPrVYb9iNo7yS7yIWpg1anfmQVNT70YVGrVub9uNw7wWu8gUYxXp35hT1GqssnBACh0o63N75VaWpbh1Ubjo96r+I7yClp6ENXrGoY26VvgiMkAWQlYD6lAEACAPhF0jVwM1pxh8slMWxtLyHNdYbbC+7f3Jkr2sPptJi80caRVwgixEjbg7dqZHVE8+qxvKyVAQAldM59etnPB+r+QBvyWzh1amivPUNC6jUrYDxdq/nBb80YhXpsIajqWMWAQAotJmk1cwAuTIclWnqy3DqoYOg2Hyx0wEjSwlznWO2xtu3d6fC9iGo02aYCyelYjPqUAQAIAEACABAECrweCR7ZHMHpGEESDJ2XHiO1NcVqhyk7WPd0R7Ut2uA2xyRZLvILCExDXdI97muGs5zswR7RJW9BqySZVdww7XbIx7WuOq5rsgT7JBRNqzTYIfYF1g7yLVjpsR7El2+AWPCkweCOR0rWD0jySZDm7PhwHYk3Vqxzk7WJ6cNBAAgAQB//9k="
            )
            .into(holder.image)*/
        /*
        Glide.with(AppController.getInstance())
                .load(student.getStudent_image())
                .into(holder.image);*/
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredresultstudentList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredresultstudentList = studentList
                } else {
                    val filteredList = ArrayList<Student>()
                    for (row in studentList!!) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.student_id!!.toLowerCase().contains(charString.toLowerCase()) || row.student_name!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredresultstudentList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredresultstudentList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredresultstudentList = filterResults.values as ArrayList<Student>
                notifyDataSetChanged()
            }
        }
    }
}
