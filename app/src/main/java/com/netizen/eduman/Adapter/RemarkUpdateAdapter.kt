package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.ResultSummary

class RemarkUpdateAdapter(private val mContext: Context, private val remarksSummaryList: List<ResultSummary>) :
    RecyclerView.Adapter<RemarkUpdateAdapter.RemarksViewHolder>() {

    inner class RemarksViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var remarks_std_id: TextView
        var remarks_std_roll: TextView
        var remarks_std_name: TextView
        var remarks_std_total_marks: TextView
        var remarks_std_grade: TextView
        var remarks_std_gpa: TextView
        var remarks_check: CheckBox
        internal var remarks_update_editText: EditText
        internal var remarksText: String


        init {
            remarks_std_id = view.findViewById<View>(R.id.remarks_update_std_id) as TextView
            remarks_std_roll = view.findViewById<View>(R.id.remarks_update_std_roll) as TextView
            remarks_std_name = view.findViewById<View>(R.id.remarks_update_std_name) as TextView
            remarks_std_total_marks = view.findViewById<View>(R.id.remarks_update_std_total_marks) as TextView
            remarks_std_grade = view.findViewById<View>(R.id.remarks_update_std_grade) as TextView
            remarks_std_gpa = view.findViewById<View>(R.id.remarks_update_std_gpa) as TextView
            remarks_check = view.findViewById<View>(R.id.remarks_update_check) as CheckBox
            remarks_update_editText = view.findViewById<View>(R.id.input_et_mark_std_remarks_update) as EditText

            remarksText = remarks_update_editText.text.toString()

            remarks_update_editText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable) {
                    remarksSummaryList[adapterPosition].result_std_remarks_des = remarks_update_editText.text.toString()
                    val remarksUpdate = remarksSummaryList[adapterPosition].result_std_remarks_des
                    val identification_id = remarksSummaryList[adapterPosition].result_std_identification

                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    remarksUpdate?.let { identification_id?.let { it1 -> dao?.UpdateRemarkDescription(it, it1) } }
                    dao?.close()
                }
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RemarkUpdateAdapter.RemarksViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.remarks_update_item_row, parent, false)
        return RemarksViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: RemarkUpdateAdapter.RemarksViewHolder, position: Int) {

        val remarks = remarksSummaryList[position]

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val Cflag = remarks.result_std_check
        val student_identification = remarks.result_std_identification

        holder.remarks_std_id.text = remarks.result_std_id
        holder.remarks_std_roll.text = remarks.result_std_roll
        holder.remarks_std_name.text = remarks.result_std_name
        holder.remarks_std_total_marks.text = remarks.result_std_total_mark
        holder.remarks_std_grade.text = remarks.result_std_grade
        holder.remarks_std_gpa.text = remarks.result_std_gpa
        holder.remarks_update_editText.setText(remarks.result_std_remarks_des)

        if (Cflag == 0) {
            holder.remarks_check.isChecked = false
        } else {
            holder.remarks_check.isChecked = true
        }

        holder.remarks_check.setOnClickListener {
            if (Cflag == 0) {
                student_identification?.let { it1 -> dao?.UpdateRemarkStudentFlag(1, it1) }
            } else {
                student_identification?.let { it1 -> dao?.UpdateRemarkStudentFlag(0, it1) }
            }
        }
        dao?.close()
    }

    override fun getItemCount(): Int {
        return remarksSummaryList.size
    }
}

