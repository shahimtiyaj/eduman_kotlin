package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import org.json.JSONException
import java.util.*

class FragmentStudentResultSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_result_search: Button? = null

    private var spinner_section: Spinner? = null
    private var spinner_exam: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null

    private var pos: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    private var txt_back: TextView? = null
    lateinit var v: View

    private var resultSummaryArrayList: ArrayList<ResultSummary>? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    internal lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.result_selection_layout, container, false)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_result_selection")
        editor.apply()

        setsectionSpinnerData()


        return v
    }

    private fun initializeViews() {
        btn_result_search = v.findViewById<View>(R.id.btn_all_result_sumry_search) as Button

        spinner_section = v.findViewById<View>(R.id.spinner_result_sumry_sec) as Spinner
        spinner_section?.onItemSelectedListener = this
        spinner_exam = v.findViewById<View>(R.id.spinner_result_sumry_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        sharedpreferences = activity!!.getSharedPreferences(
            MyPREFERENCES, 0
        )
        accessToken = sharedpreferences.getString("accessToken", null)


        btn_result_search?.setOnClickListener {
            try {
                if (localExamID == null || localSectionID == null) {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.not_found,
                            "Failure",
                            "Please Fill up the required fields."
                        )
                    }
                } else {

                    val senrollmentFragment = FragmentStudentResult()
                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                    fragmentTransaction.commit()
            }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_result_sumry_sec) {
                selectSection = spinner_section?.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    resultSummaryArrayList?.clear()

                    pos = position.toString()

                    selectSection = parent.getItemAtPosition(position).toString()
                    val da = DAO(activity!!)
                    da.open()
                    localSectionID = da.GetSectionID(selectSection!!)
                    loadServerExamData(pos!!)
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_result_sumry_exam) {
                selectExam = spinner_exam!!.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    examArrayList.clear()

                    selectExam = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localExamID = da.GetExamResultID(selectExam!!, pos!!)

                    da.close()
                }
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result  ", "Spinner data")
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val studentResult = dao?.allSectionData


            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in studentResult?.indices!!) {
                studentResult[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(
                activity, android.R.layout.simple_spinner_item,
                section
            )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result sec  ", "Spinner data")
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, exam)
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student Result Exam ", "Spinner data")
        }
    }

    private fun loadServerExamData(pos: String) {
        val hitURL =
            AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID
        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(c.getString("examConfigId"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM_RESULT + "(SectionPosition, ExamId, ExamName) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(pos, c.getString("examConfigId"), jobj.getString("name"))
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Student Result Exam", e.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {

        private val TAG = "FragmentStudentResult"
         var localSectionID: String? = null
         var localExamID: String? = null
    }

}
