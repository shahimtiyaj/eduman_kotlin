package com.netizen.eduman

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Context


class FragmentDashboard : Fragment() {

    private var studentCard: CardView? = null
    private var hrCard: CardView? = null
    private var attendanceCard: CardView? = null
    private var semesterExamCard: CardView? = null
    lateinit var v: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.home_page_layout, container, false)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        studentCard = v.findViewById<View>(R.id.studentcardId) as CardView
        hrCard = v.findViewById<View>(R.id.hrcardId) as CardView
        attendanceCard = v.findViewById<View>(R.id.attendancecardId) as CardView
        semesterExamCard = v.findViewById<View>(R.id.semesterExamcardId) as CardView

        activity?.title = Html.fromHtml("<font color='#2F3292'>HOME</font>")

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "home")
        editor.apply()

        OnclickView()
        return v
    }

    fun OnclickView() {
        studentCard?.setOnClickListener {
            try {
                val stdtabFragment = FragmentSEnrollmentForm()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        hrCard?.setOnClickListener {
            val hrmtabFragment = FragmentHRnrollmentForm()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, hrmtabFragment)
            fragmentTransaction.commit()
        }

        attendanceCard?.setOnClickListener {
            val atdtDashFragment = FragmentTakeAtdSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, atdtDashFragment)
            fragmentTransaction.commit()
        }

        semesterExamCard?.setOnClickListener(View.OnClickListener {
            val fragmentSemesterExam = FragmentSemesterExamDash()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentSemesterExam)
            fragmentTransaction.commit()
        })
    }
}
