package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.MarkDistributionAdapter
import com.netizen.eduman.Adapter.UpdateMarkAdapter
import com.netizen.eduman.FragmentUpdateMarkSelection.Companion.localExamID
import com.netizen.eduman.FragmentUpdateMarkSelection.Companion.localGroupID
import com.netizen.eduman.FragmentUpdateMarkSelection.Companion.localSectionID
import com.netizen.eduman.FragmentUpdateMarkSelection.Companion.localSubjectID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_INPUT_STUDENT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_SCALE_DISTRIBUTION
import com.netizen.eduman.model.InputMark
import com.netizen.eduman.model.MarkDistribution
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*
import kotlin.NullPointerException as NullPointerException1


class FragmentUpdateMark : Fragment() {
    private var btn_input_mark_save: Button? = null

    private var v: View? = null

    private var recyclerView: RecyclerView? = null
    private var recyclerView1: RecyclerView? = null
    private var adapter: UpdateMarkAdapter? = null
    private var adapter1: MarkDistributionAdapter? = null

    private var inputMarkArrayList: ArrayList<InputMark>? = null
    private var markDistributionArrayList: ArrayList<MarkDistribution>? = null

    private var mLayoutManager: LinearLayoutManager? = null
    private var mLayoutManager1: LinearLayoutManager? = null
    private var jObjPost: String? = null
    private var txt_back: TextView? = null


    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.mark_update_recyler_list_layout, container, false)
        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "update_mark_std_details")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()
        GetMarkScaleDistrubution()
        GetMarkStudentList()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_input_mark_save = v?.findViewById<View>(R.id.btn_update_mark_save) as Button


        btn_input_mark_save?.setOnClickListener {
            try {
                if (inputMarkArrayList?.isNotEmpty()!!) {
                    markUpdateDataSendToServer()
                } else {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.fail,
                            "Failure",
                            "Sorry ! Empty list."
                        )
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = TabFragmentMIU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun recyclerViewInit() {

        // Initialize item list
        inputMarkArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.mark_input_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = UpdateMarkAdapter(activity!!, inputMarkArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter

        // myDialog.setContentView(R.layout.mark_distribution_recyler_list);

        // Initialize item list
        markDistributionArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView1 = v?.findViewById<View>(R.id.mark_disbution_recyle_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter1 = MarkDistributionAdapter(activity!!, markDistributionArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager1 = LinearLayoutManager(context)
        // Set layout manager to position the items
        recyclerView1?.layoutManager = mLayoutManager1
        // Set the default animator
        recyclerView1?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView1?.adapter = adapter1
    }

    fun GetMarkScaleDistrubution() {
        //final String hitURL = AppController.BaseUrl + "exam/mark/config/list/by/subject-id?access_token=c54ff645-c423-4300-9414-450a9e22cd5b&classConfigurationId=100175&groupId=3000442105&subjectId=1001492202&examConfigId=62";
        //val hitURL = AppController.BaseUrl + "exam/mark/config/list/by/subject-id?access_token=eac968c5-c292-4638-8f80-5f956ec34095&classConfigurationId=143189&groupId=3000432105&subjectId=1001492202&examConfigId=76"

        val hitURL = AppController.BaseUrl + "exam/mark/config/list/by/subject-id?access_token=" + accessToken +
                "&classConfigurationId=" + localSectionID + "&groupId=" + localGroupID + "&subjectId=" + localSubjectID + "&examConfigId=" + localExamID

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val markDistribution = MarkDistribution()

                        markDistribution.exameCode = c.getString("shortCodeName")
                        markDistribution.totalMark = c.getString("totalMark")
                        markDistribution.passMark = c.getString("passMark")
                        markDistribution.acceptance = c.getString("acceptance")

                        markDistributionArrayList!!.add(markDistribution)

                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_MARK_SCALE_DISTRIBUTION + "(shortCodeName, totalMark, " +
                                    "passMark, acceptance, defaultId) " +
                                    "VALUES(?, ?, ?, ?, ?)"),

                            arrayOf(
                                c.getString("shortCodeName"),
                                c.getString("totalMark"),
                                c.getString("passMark"),
                                c.getString("acceptance"),
                                c.getString("defaultId")
                            )
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                //Notify adapter if data change
                adapter1!!.notifyDataSetChanged()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }


    fun GetMarkStudentList() {

        val hitURL =
            AppController.BaseUrl + "exam/mark/student/list?access_token=" + accessToken + "&classConfigurationId=" + localSectionID +
                    "&groupId=" + localGroupID + "&subjectId=" + localSubjectID + "&examConfigurationId=" + localExamID

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val inputMark = InputMark()

                        inputMark.in_mark_st_identification_id = c.getString("identificationId")
                        inputMark.in_mark_st_id = c.getString("customStudentId")
                        inputMark.in_mark_st_roll = c.getString("studentRoll")
                        inputMark.in_mark_st_name = c.getString("studentName")
                        inputMark.shortCode1 = c.getString("shortCode1")
                        inputMark.shortCode2 = c.getString("shortCode2")
                        inputMark.shortCode3 = c.getString("shortCode3")

                        inputMarkArrayList!!.add(inputMark)

                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_MARK_INPUT_STUDENT + "(identificationId, customStudentId, studentRoll, " +
                                    "studentName, shortCode1, shortCode2, shortCode3) " +
                                    "VALUES(?, ?, ?, ?, ?, ?, ?)"),

                            arrayOf(
                                c.getString("identificationId"),
                                c.getString("customStudentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("shortCode1"),
                                c.getString("shortCode2"),
                                c.getString("shortCode3")
                            )
                        )

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    fun markUpdateDataSendToServer() {
        // String hitURL = "http://192.168.31.14:8080/exam/mark/input?access_token=c54ff645-c423-4300-9414-450a9e22cd5b"; // zakir bhai IP
        val hitURL = AppController.BaseUrl + "exam/mark/input?access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Sending Update mark ..") }

        InputMarkJsonData()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Update Mark Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.success,
                                "Update Mark",
                                "Update mark successfully sent"
                            )
                        }
                    } else {
                        activity?.let { AppController.instance?.Alert(it, R.drawable.fail, "Input mark", status) }
                        activity?.let { AppController.instance?.hideProgress(it) }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
        activity?.let { AppController.instance?.hideProgress(it) }

    }


    private fun InputMarkJsonData() {
        inputMarkArrayList = ArrayList()

        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val inputMarks = dao?.getALLInputMark()

            val jsonObj = JSONObject()
            val jsonArray = JSONArray()

            for (inputMark in inputMarks!!) {
                val arrayItem = JSONObject()

                arrayItem.put("identificationId", inputMark.in_mark_st_identification_id)
                arrayItem.put("shortCode2", inputMark.shortCode2)
                arrayItem.put("shortCode3", inputMark.shortCode3)
                arrayItem.put("markinputId", "45569158")

                jsonArray.put(arrayItem)

            }

            try {
                jsonObj.put("examMarkInputRqHelpers", jsonArray)
                jsonObj.put("examConfigurationId", localExamID)//76
                jsonObj.put("classConfigurationId", localSectionID)//4299712
                jsonObj.put("groupId", localGroupID) //3000432105
                jsonObj.put("subjectId", localSubjectID)//1001492202
                jsonObj.put("type", "update")

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            jObjPost = jsonObj.toString()
            Log.d("Input Mark Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        private val TAG = "FragmentUpdateMark"
    }

}
