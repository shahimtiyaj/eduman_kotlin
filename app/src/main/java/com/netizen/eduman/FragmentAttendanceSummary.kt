package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.AttendanceSummaryAdapter
import com.netizen.eduman.FragmentSAttendanceSelection.Companion.dateOfAttendanceSummary
import com.netizen.eduman.FragmentSAttendanceSelection.Companion.localPeriodID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.AttendanceSummary
import com.netizen.eduman.model.Period
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.*


class FragmentAttendanceSummary : Fragment(), AdapterView.OnItemSelectedListener {
    private var spinner_attendance_summary_period: Spinner? = null

    private var selectPeriod: String? = null
    private var v: View? = null

    private var recyclerView: RecyclerView? = null
    private var adapter: AttendanceSummaryAdapter? = null
    private var attendancesSummaryArrayList: ArrayList<AttendanceSummary>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var total_found: TextView? = null
    private var txt_back: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.attendance_summary_list_view, container, false)

        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_take_atd_summary")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()
        GetStudentAttendanceSummary()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!

        accessToken = sharedpreferences.getString("accessToken", null)

        total_found = v?.findViewById<View>(R.id.total_found) as TextView
        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentSAttendanceSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    fun recyclerViewInit() {
        // Initialize item list
        attendancesSummaryArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_attendance_summary_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = AttendanceSummaryAdapter(activity!!, attendancesSummaryArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }


    fun GetStudentAttendanceSummary() {

        val hitURL =
            AppController.BaseUrl + "attendance/report/sec-wise/atten/summary/by-date?attendanceDate=" + dateOfAttendanceSummary + "&periodId=" + localPeriodID + "&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val attendanceSummary = AttendanceSummary()
                        attendanceSummary.atd_smry_date = c.getString("attendanceDate")
                        attendanceSummary.atd_smry_section_id = c.getString("classConfigId")

                        attendanceSummary.atd_smry_section = c.getString("sectionName")
                        attendanceSummary.atd_smry_total_student = c.getString("totalStds")
                        attendanceSummary.atd_smry_present = c.getString("presentStds")
                        attendanceSummary.atd_smry_absent = c.getString("absentStds")
                        attendanceSummary.atd_smry_leave = c.getString("leaveStds")

                        attendanceSummary.atd_smry_total_present_per = c.getString("totalPresentPercent")
                        attendanceSummary.atd_smry_total_absent_per = c.getString("totalAbsentPercent")
                        attendanceSummary.atd_smry_total_leave_per = c.getString("totalLeavePercent")

                        // adding item to ITEM list
                        attendancesSummaryArrayList?.add(attendanceSummary)
                        /*
                                        DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_TAKEATTENDANCE + "(studentId, studentRoll, studentName, studentGender, identificationId) " +
                                                        "VALUES(?, ?, ?, ?, ?)",
                                                new String[]{c.getString("studentId"), c.getString("studentRoll"), c.getString("studentName"),
                                                        c.getString("studentGender"), c.getString("identificationId")});*/
                    }

                    if (attendancesSummaryArrayList?.isEmpty()!!) {
                        Log.d("Attendance: summary", "Attendance summary data found")
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.fail,
                                "Failure",
                                "Sorry! no data found"
                            )
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }
                total_found?.setText(attendancesSummaryArrayList?.size.toString())
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setperiodSpinnerData(periodsArrayList: ArrayList<Period>) {
        try {
            val period = ArrayList<String>()
            period.add(resources.getString(R.string.select_period))
            for (i in periodsArrayList.indices) {
                periodsArrayList[i].getPeriodName()?.let { period.add(it) }
            }

            val periodAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, period)
            periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_attendance_summary_period?.adapter = periodAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Atd Summary exam ", "Period spinner data")
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_attendance_summary_period) {
                selectPeriod = spinner_attendance_summary_period!!.selectedItem.toString()
                if (selectPeriod == resources.getString(R.string.select_period)) {
                    selectPeriod = ""
                } else {
                    selectPeriod = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localPeriodID = da.GetAtSummaryPeriodID(selectPeriod!!)
                    //inputAttendanceSummaryDate?.getText()?.clear()
                    // dateOfAttendanceSummary = inputAttendanceSummaryDate?.text.toString()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Atd Summary exam ", "Period spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    companion object {
        private val TAG = "FragmentAttendanceSummary"
    }
}
