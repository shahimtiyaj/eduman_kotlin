package com.netizen.eduman

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.Period
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.io.UnsupportedEncodingException
import java.lang.NullPointerException
import java.util.*

class FragmentAttdncCount : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    lateinit var v: View

    private var btn_attd_count_process: Button? = null
    private var spinner_section: Spinner? = null
    private var spinner_exam: Spinner? = null
    private var spinner_period: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null
    private var selectPeriod: String? = null
    private var localPeriodID: String? = null
    private var localSectionID: String? = null
    private var localExamID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()
    internal var periodsArrayList = ArrayList<Period>()


    private val jObjPost: String? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.attendance_count_layout, container, false)
        initializeViews()
        try {
            loadServerSectionData()

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance count ", "Spinner data")
        }

        return v
    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_attd_count_process = v.findViewById<View>(R.id.btn_attd_count_process) as Button

        spinner_section = v.findViewById<View>(R.id.spinner_attd_count_section) as Spinner
        spinner_section?.onItemSelectedListener = this

        spinner_period = v.findViewById<View>(R.id.spinner_attd_count_period) as Spinner
        spinner_period?.onItemSelectedListener = this

        spinner_exam = v.findViewById<View>(R.id.spinner_attd_count_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        btn_attd_count_process!!.setOnClickListener {
            try {
                if (localExamID == null || localSectionID == null || localPeriodID == null) {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.requred_alert,
                            "Failure",
                            "Please Fill up the required fields."
                        )
                    }
                } else {
                    attdCountProcessDataSendToServer()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_attd_count_section) {
                selectSection = spinner_section!!.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    selectSection = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localSectionID = da.GetSectionID(selectSection!!)
                    loadServerPeriodData()
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_attd_count_period) {
                selectPeriod = spinner_period!!.selectedItem.toString()
                if (selectPeriod == resources.getString(R.string.select_period)) {
                    selectPeriod = ""
                } else {
                    selectPeriod = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localPeriodID = da.GetAtSummaryPeriodID(selectPeriod!!)
                    Log.d("Period ID: ", localPeriodID)
                    loadServerExamData()
                    da.close()
                }
            }


            if (spinner.id == R.id.spinner_attd_count_exam) {
                selectExam = spinner_exam!!.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    selectExam = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localExamID = da.GetExamID(selectExam!!)
                    da.close()
                }
            }

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance count ", "Seleted Spinner data")
        }

    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>) {
        try {
            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in sectionArrayList.indices) {
                sectionArrayList[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(
                Objects.requireNonNull<FragmentActivity>(activity),
                android.R.layout.simple_spinner_item,
                section
            )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance count ", " Section Spinner data")
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setperiodSpinnerData(periodsArrayList: ArrayList<Period>) {
        try {
            val period = ArrayList<String>()
            period.add(resources.getString(R.string.select_period))
            for (i in periodsArrayList.indices) {
                periodsArrayList[i].getPeriodName()?.let { period.add(it) }
            }

            val periodAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, period)
            periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_period?.adapter = periodAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance count ", " period Spinner data")
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {
            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter =
                ArrayAdapter(
                    Objects.requireNonNull<FragmentActivity>(activity),
                    android.R.layout.simple_spinner_item,
                    exam
                )
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance count ", " exam Spinner data")
        }
    }


    private fun loadServerSectionData() {
        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_SECTION + "(classConfigId, classShiftSection) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("classConfigId"), c.getString("classShiftSection"))
                            )

                            if (!sectionsArrayList.isEmpty()) {
                                setsectionSpinnerData(sectionsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error ->
                    Log.d("Atd Count: Section", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    private fun loadServerExamData() {
        val hitURL =
            AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID!!
        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Log.d("Atd Count: Exam", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerPeriodData() {
        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2301"

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        periodsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val periodlist = Period()
                            periodlist.setPeriodId(c.getString("typeId"))
                            periodlist.setPeriodName(c.getString("name"))

                            periodsArrayList.add(periodlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_ATTENDANCESUMMARYPERIOD + "(typeId, periodName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("typeId"), c.getString("name"))
                            )

                            if (!periodsArrayList.isEmpty()) {
                                setperiodSpinnerData(periodsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                      //  Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                  //  Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    fun attdCountProcessDataSendToServer() {
        val hitURL =
            AppController.BaseUrl + "result/process/working-day/generate?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&periodId=" + localPeriodID + "&examConfigurationId=" + localExamID + "&type=insert"
        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "General exam result processing..") }

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.success,
                                "Attendance Count",
                                "Attendance Count successfully done"
                            )
                        }
                        activity?.let { AppController.instance?.hideProgress(it) }
                    } else {
                        activity?.let { AppController.instance?.Alert(it, R.drawable.fail, "Attendance Count", status) }

                        activity?.let { AppController.instance?.hideProgress(it) }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return jObjPost?.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {
        private val TAG = "FragmentGeneralExam"
    }

}
