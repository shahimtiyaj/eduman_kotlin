package com.netizen.eduman

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class FragmentAttdanceDash : Fragment() {

    private var studentATDCard: CardView? = null
    private var hrATDCard: CardView? = null
    private var attendanceBackCard: CardView? = null
    private var atd_dash_count_cardId: CardView? = null
    lateinit var v: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.attendance_dashboard_layput, container, false)
        studentATDCard = v.findViewById<View>(R.id.atd_dash_std_cardId) as CardView
        hrATDCard = v.findViewById<View>(R.id.atd_dash_hr_cardId) as CardView
        atd_dash_count_cardId = v.findViewById<View>(R.id.atd_dash_count_cardId) as CardView
        attendanceBackCard = v.findViewById<View>(R.id.back_from_atd_dash) as CardView

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        OnclickView()
        return v
    }

    fun OnclickView() {
        studentATDCard!!.setOnClickListener {
            try {
                val stdtabFragment = TabFragmentATD()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        hrATDCard!!.setOnClickListener {
            val hrmtabFragment = TabFragmentHPAL()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, hrmtabFragment)
            fragmentTransaction.commit()
        }

        atd_dash_count_cardId!!.setOnClickListener {
            val fragmentAttdncCount = FragmentAttdncCount()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentAttdncCount)
            fragmentTransaction.commit()
        }

        attendanceBackCard!!.setOnClickListener {
            val dashFragment = FragmentDashboard()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, dashFragment)
            fragmentTransaction.commit()
        }

    }
}
