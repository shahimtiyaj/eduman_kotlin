package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.HRListAdapter
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.HR
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.*

class FragmentHRList : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var adapter: HRListAdapter? = null
    private var hrsArrayList: ArrayList<HR>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var txt_back: TextView? = null
    private var et_hr_id_search: EditText? = null


    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_recyler_listview, container, false)
        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_list")
        editor.apply()

        recyclerViewInit()
        GetAllHRDataWebService()
        return v
    }

    private fun initializeViews() {

        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)
        txt_back = v?.findViewById<View>(R.id.back_text) as TextView
        activity?.setTitle(Html.fromHtml("<font color='#2F3292'>HR List</font>"))


        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentHRnrollmentForm()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        et_hr_id_search = v?.findViewById<View>(R.id.et_hr_id_search) as EditText


        et_hr_id_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    fun recyclerViewInit() {

        // Initialize item list
        hrsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_hr_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = HRListAdapter(activity!!, hrsArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter

    }

    fun GetAllHRDataWebService() {
        val hitURL = AppController.BaseUrl + "staff/basic/list/with/photo?&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }


        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val hrlist = HR()
                        //set the json data in the model
                        hrlist.setHr_id(c.getString("staffId"))
                        hrlist.setHr_name(c.getString("staffName"))
                        hrlist.setHr_gender(c.getString("gender"))
                        hrlist.setHr_religion(c.getString("staffReligion"))
                        hrlist.setHr_category(c.getString("staffCategory"))
                        hrlist.setHr_designation(c.getString("designationName"))
                        hrlist.setHr_blood_grp(c.getString("bloodGroup"))
                        hrlist.setHr_phone(c.getString("staffMobile1"))
                        hrlist.setHr_image(c.getString("image"))

                        // adding item to ITEM list
                        hrsArrayList!!.add(hrlist)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }

                if (hrsArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(
                        activity!!,
                        R.drawable.not_found,
                        "Failure",
                        "Sorry no data found."
                    )
                }

                //Notify adapter if data change
                adapter?.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                //headers.put("access_token", "7e1bb8bc-c8ef-4821-8811-97a1eef7135c");

                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {

        private val TAG = "FragmentHRList"
    }

}

