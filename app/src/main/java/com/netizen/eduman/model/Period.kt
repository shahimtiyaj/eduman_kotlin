package com.netizen.eduman.model

class Period {
    private var periodId: String? = null
    private var periodName: String? = null

    fun getPeriodId(): String? {
        return periodId
    }

    fun setPeriodId(periodId: String) {
        this.periodId = periodId
    }

    fun getPeriodName(): String? {
        return periodName
    }

    fun setPeriodName(periodName: String) {
        this.periodName = periodName
    }
}