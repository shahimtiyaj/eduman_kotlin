package com.netizen.eduman.model

class AcademicYear {
    private var yearId: String? = null
    private var academicYear: String? = null

    constructor() {}

    constructor(academicYear: String) {
        this.academicYear = academicYear
    }


    fun getYearId(): String? {
        return yearId
    }

    fun setYearId(yearId: String) {
        this.yearId = yearId
    }

    fun getAcademicYear(): String? {
        return this.academicYear
    }

    fun setAcademicYear(academicYear: String) {
        this.academicYear = academicYear
    }
}