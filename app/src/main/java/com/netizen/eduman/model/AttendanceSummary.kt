package com.netizen.eduman.model

class AttendanceSummary {
    var atd_smry_date: String? = null
    var atd_smry_section_id: String? = null


    var atd_smry_section: String? = null
    var atd_smry_total_student: String? = null
    var atd_smry_present: String? = null
    var atd_smry_absent: String? = null
    var atd_smry_leave: String? = null

    var atd_smry_total_present_per: String? = null
    var atd_smry_total_absent_per: String? = null
    var atd_smry_total_leave_per: String? = null

    fun atd_smry_section(): String? {
        return atd_smry_section
    }

    fun atd_smry_section(atd_smry_section: String) {
        this.atd_smry_section = atd_smry_section
    }

    fun atd_smry_total_student(): String? {
        return atd_smry_total_student
    }

    fun atd_smry_total_student(atd_smry_total_student: String) {
        this.atd_smry_total_student = atd_smry_total_student
    }

    fun atd_smry_present(): String? {
        return atd_smry_present
    }

    fun atd_smry_present(atd_smry_present: String) {
        this.atd_smry_present = atd_smry_present
    }

    fun atd_smry_absent(): String? {
        return atd_smry_absent
    }

    fun atd_smry_absent(atd_smry_absent: String) {
        this.atd_smry_absent = atd_smry_absent
    }

    fun atd_smry_leave(): String? {
        return atd_smry_leave
    }

    fun atd_smry_leave(atd_smry_leave: String) {
        this.atd_smry_leave = atd_smry_leave
    }

    fun atd_smry_total_present_per(): String? {
        return atd_smry_total_present_per
    }

    fun atd_smry_total_present_per(atd_smry_total_present_per: String) {
        this.atd_smry_total_present_per = atd_smry_total_present_per
    }

    fun atd_smry_total_absent_per(): String? {
        return atd_smry_total_absent_per
    }

    fun atd_smry_total_absent_per(atd_smry_total_absent_per: String) {
        this.atd_smry_total_absent_per = atd_smry_total_absent_per
    }

    fun atd_smry_total_leave_per(): String? {
        return atd_smry_total_leave_per
    }

    fun atd_smry_total_leave_per(atd_smry_total_leave_per: String) {
        this.atd_smry_total_leave_per = atd_smry_total_leave_per
    }

    fun atd_smry_date(): String? {
        return atd_smry_date
    }

    fun atd_smry_date(atd_smry_date: String) {
        this.atd_smry_date = atd_smry_date
    }

    fun atd_smry_section_id(): String? {
        return atd_smry_section_id
    }

    fun atd_smry_section_id(atd_smry_section_id: String) {
        this.atd_smry_date = atd_smry_section_id
    }
}
