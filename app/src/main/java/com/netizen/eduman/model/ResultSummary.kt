package com.netizen.eduman.model

class ResultSummary {


    var result_std_identification: String? = null
    var result_std_id: String? = null
    var result_std_roll: String? = null
    var result_std_name: String? = null
    var result_std_total_mark: String? = null
    var result_std_grade: String? = null
    var result_std_gpa: String? = null
    var result_std_status: String? = null
    var result_std_sec_merit: String? = null
    var result_std_no_failed: String? = null
    var result_std_check: Int = 0

    var result_std_remarks_id: String? = null
    var result_std_remarks_des: String? = null
    var result_std_remarks_title: String? = null


    //default
    constructor() {

    }

    // result and assign remarks
    constructor(
        result_std_identification: String, result_std_id: String, result_std_roll: String, result_std_name: String,
        result_std_total_mark: String, result_std_grade: String, result_std_gpa: String, check: Int
    ) {
        this.result_std_identification = result_std_identification
        this.result_std_id = result_std_id
        this.result_std_roll = result_std_roll
        this.result_std_name = result_std_name
        this.result_std_total_mark = result_std_total_mark
        this.result_std_grade = result_std_grade
        this.result_std_gpa = result_std_gpa
        this.result_std_check = check
    }

    //update remarks
    constructor(
        result_std_identification: String, result_std_id: String, result_std_roll: String, result_std_name: String,
        result_std_total_mark: String, result_std_grade: String, result_std_gpa: String, result_std_remarks_id: String,
        result_std_remarks_des: String, result_std_remarks_title: String, check: Int
    ) {
        this.result_std_identification = result_std_identification
        this.result_std_id = result_std_id
        this.result_std_roll = result_std_roll
        this.result_std_name = result_std_name
        this.result_std_total_mark = result_std_total_mark
        this.result_std_grade = result_std_grade
        this.result_std_gpa = result_std_gpa
        this.result_std_remarks_id = result_std_remarks_id
        this.result_std_remarks_des = result_std_remarks_des
        this.result_std_remarks_title = result_std_remarks_title
        this.result_std_check = check

    }


    fun result_std_identification(): String? {
        return result_std_identification
    }

    fun result_std_identification(result_std_identification: String) {
        this.result_std_identification = result_std_identification
    }

    fun result_std_id(): String? {
        return result_std_id
    }

    fun result_std_id(result_std_id: String) {
        this.result_std_id = result_std_id
    }

    fun result_std_roll(): String? {
        return result_std_roll
    }

    fun result_std_roll(result_std_roll: String) {
        this.result_std_roll = result_std_roll
    }

    fun result_std_name(): String? {
        return result_std_name
    }

    fun result_std_name(result_std_name: String) {
        this.result_std_name = result_std_name
    }

    fun result_std_total_mark(): String? {
        return result_std_total_mark
    }

    fun result_std_total_mark(result_std_total_mark: String) {
        this.result_std_total_mark = result_std_total_mark
    }

    fun result_std_grade(): String? {
        return result_std_grade
    }

    fun result_std_grade(result_std_grade: String) {
        this.result_std_grade = result_std_grade
    }

    fun result_std_gpa(): String? {
        return result_std_gpa
    }

    fun result_std_gpa(result_std_gpa: String) {
        this.result_std_gpa = result_std_gpa
    }

    fun result_std_status(): String? {
        return result_std_status
    }

    fun result_std_status(result_std_status: String) {
        this.result_std_status = result_std_status
    }

    fun result_std_sec_merit(): String? {
        return result_std_sec_merit
    }

    fun result_std_sec_merit(result_std_sec_merit: String) {
        this.result_std_sec_merit = result_std_sec_merit
    }

    fun result_std_no_failed(): String? {
        return result_std_no_failed
    }

    fun result_std_no_failed(result_std_no_failed: String) {
        this.result_std_no_failed = result_std_no_failed
    }

    fun result_std_check(): Int {
        return result_std_check
    }

    fun result_std_check(result_std_check: Int) {
        this.result_std_check = result_std_check
    }

    fun result_std_remarks_id(): String? {
        return result_std_remarks_id
    }

    fun result_std_remarks_id(result_std_remarks_id: String) {
        this.result_std_remarks_id = result_std_remarks_id
    }

    fun result_std_remarks_des(): String? {
        return result_std_remarks_des
    }

    fun result_std_remarks_des(result_std_remarks_des: String) {
        this.result_std_remarks_des = result_std_remarks_des
    }

    fun result_std_remarks_title(): String? {
        return result_std_remarks_title
    }

    fun result_std_remarks_title(result_std_remarks_title: String) {
        this.result_std_remarks_title = result_std_remarks_title
    }
}
