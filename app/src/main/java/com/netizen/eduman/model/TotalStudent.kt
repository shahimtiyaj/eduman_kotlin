package com.netizen.eduman.model

class TotalStudent {
    private var std_id: String? = null
    private var std_roll: String? = null
    private var std_name: String? = null
    private var std_gender: String? = null
    private var std_mobile_no: String? = null
    private var std_status: String? = null

    fun getStd_id(): String? {
        return std_id
    }

    fun setStd_id(std_id: String) {
        this.std_id = std_id
    }

    fun getStd_roll(): String? {
        return std_roll
    }

    fun setStd_roll(std_roll: String) {
        this.std_roll = std_roll
    }

    fun getStd_name(): String? {
        return std_name
    }

    fun setStd_name(std_name: String) {
        this.std_name = std_name
    }

    fun getStd_gender(): String? {
        return std_gender
    }

    fun setStd_gender(std_gender: String) {
        this.std_gender = std_gender
    }

    fun getStd_mobile_no(): String? {
        return std_mobile_no
    }

    fun setStd_mobile_no(std_mobile_no: String) {
        this.std_mobile_no = std_mobile_no
    }

    fun getStd_status(): String? {
        return std_status
    }

    fun setStd_status(std_status: String) {
        this.std_status = std_status
    }

}