package com.netizen.eduman.model

class Student {
     var student_id: String? = null
     var student_roll: String? = null
     var student_name: String? = null
     var group: String? = null
     var category: String? = null
     var gender: String? = null
     var religion: String? = null
     var father_name: String? = null
     var mother_name: String? = null
     var mobile_no: String? = null
     var student_image: String? = null

    fun getstudent_id(): String? {
        return student_id
    }

    fun setstudent_id(student_id: String) {
        this.student_id = student_id
    }

    fun getstudent_roll(): String? {
        return student_roll
    }

    fun setstudent_roll(student_roll: String) {
        this.student_roll = student_roll
    }

    fun getstudent_name(): String? {
        return student_name
    }

    fun setstudent_name(student_name: String) {
        this.student_name = student_name
    }

    fun getgroup(): String? {
        return group
    }

    fun setgroup(group: String) {
        this.group = group
    }

    fun getcategory(): String? {
        return category
    }

    fun setcategory(category: String) {
        this.category = category
    }

    fun getgender(): String? {
        return gender
    }

    fun setgender(gender: String) {
        this.gender = gender
    }

    fun getreligion(): String? {
        return religion
    }

    fun setreligion(religion: String) {
        this.religion = religion
    }

    fun getfather_name(): String? {
        return father_name
    }

    fun setfather_name(father_name: String) {
        this.father_name = father_name
    }

    fun getmother_name(): String? {
        return mother_name
    }

    fun setmother_name(mother_name: String) {
        this.mother_name = mother_name
    }

    fun getmobile_no(): String? {
        return mobile_no
    }

    fun setmobile_no(mobile_no: String) {
        this.mobile_no = mobile_no
    }

    fun getstudent_image(): String? {
        return student_image
    }

    fun setstudent_image(student_image: String) {
        this.student_image = student_image
    }
}