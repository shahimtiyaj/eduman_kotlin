package com.netizen.eduman.model

class UserLogin() {
    private var userId: String? = null
    private var userName: String? = null
    private var userPassword: String? = null

    constructor(userId: String, userName: String, userPassword: String) : this() {
        this.userId = userId
        this.userName = userName
        this.userPassword = userPassword
    }

    fun getuserId(): String? {
        return userName
    }

    fun setuserId(userName: String) {
        this.userName = userName
    }

    fun getuserName(): String? {
        return userName
    }

    fun setuserName(userName: String) {
        this.userName = userName
    }

    fun getuserPassword(): String? {
        return userPassword
    }

    fun setuserPassword(userPassword: String) {
        this.userPassword = userPassword
    }
}