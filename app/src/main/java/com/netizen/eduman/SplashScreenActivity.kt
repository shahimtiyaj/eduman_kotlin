package com.netizen.eduman

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFullScreenWindow()
        setContentView(R.layout.splash_layout)
        viewInitialization()
        splashThread()
    }

    private fun requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    private fun viewInitialization() {
        val imageView = findViewById<View>(R.id.imageView)
        val image = findViewById<View>(R.id.image)

        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.fade)
        //val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_out)

        imageView.startAnimation(anim)
        //imageView.startAnimation(animation)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

    }

    private fun splashThread() {
        val timer = object : Thread() {
            override fun run() {
                try {
                    sleep(3000)
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.left_in, R.anim.left_out)
                    finish()
                    super.run()
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }
        timer.start()
    }
}

