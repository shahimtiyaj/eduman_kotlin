package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.io.UnsupportedEncodingException
import java.util.*

class FragmentMeritProcess : Fragment(), AdapterView.OnItemSelectedListener {

    private var v: View? = null

    private var btn_merit_process: Button? = null
    private var spinner_section: Spinner? = null
    private var spinner_exam: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null
    private val localPeriodID: String? = null
    private var localSectionID: String? = null
    private var localExamID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    private var txt_back: TextView? = null


    private val jObjPost: String? = null
    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.merit_position_process, container, false)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "merit_position_process")
        editor.apply()

        setsectionSpinnerData()

        return v

    }

    private fun initializeViews() {
        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_merit_process = v?.findViewById<View>(R.id.btn_grand_merit_pos_process) as Button

        spinner_section = v?.findViewById<View>(R.id.spinner_merit_process_class) as Spinner
        spinner_section?.onItemSelectedListener = this


        spinner_exam = v?.findViewById<View>(R.id.spinner_merit_process_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this


        btn_merit_process?.setOnClickListener {
            try {

                if (localExamID == null || localSectionID == null) {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.requred_alert,
                            "Failure",
                            "Please Fill up the required fields."
                        )
                    }
                } else {
                    meritPositionProcessDataSendToServer()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val spinner = parent as Spinner
        // val spinner = parent as SearchableSpinner

        if (spinner.id == R.id.spinner_merit_process_class) {
            selectSection = spinner_section?.selectedItem.toString()
            if (selectSection == resources.getString(R.string.select_section)) {
                selectSection = ""
            } else {
                selectSection = parent.getItemAtPosition(position).toString()

                val da = DAO(activity!!)
                da.open()
                localSectionID = da.GetSectionID(selectSection!!)
                loadServerExamData()
                da.close()
            }
        }


        if (spinner.id == R.id.spinner_merit_process_exam) {
            selectExam = spinner_exam?.selectedItem.toString()
            if (selectExam == resources.getString(R.string.select_exam)) {
                selectExam = ""
            } else {
                selectExam = parent.getItemAtPosition(position).toString()

                val da = DAO(activity!!)
                da.open()
                localExamID = da.GetExamID(selectExam!!)
                da.close()
            }
        }


    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val meritposition = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in meritposition?.indices!!) {
                meritposition[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            Log.d("General exam ", "Section spinner data")
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, exam)
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            Log.d("General exam ", "Section spinner data")
        }
    }

    private fun loadServerExamData() {
        val hitURL =
            AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID!!
        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("Merit Final: Section", e.toString())
                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Grand Final: Section", e.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    fun meritPositionProcessDataSendToServer() {
        val hitURL =
            AppController.BaseUrl + "result/process/merit-position/generate?access_token=" + accessToken + "&classConfigurationId=" + localSectionID + "&examConfigurationId=" + localExamID + "&type=insert"
        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Merit position processing...") }

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.success,
                                "Merit Position",
                                "Merit Position successfully processed"
                            )
                        }
                        activity?.let { AppController.instance?.hideProgress(it) }

                    } else {
                        activity?.let { AppController.instance?.Alert(it, R.drawable.fail, "Merit Position", status) }
                        activity?.let { AppController.instance?.hideProgress(it) }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }


                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return jObjPost?.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }
            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {
        private val TAG = "FragmentGeneralExam"
    }

}
