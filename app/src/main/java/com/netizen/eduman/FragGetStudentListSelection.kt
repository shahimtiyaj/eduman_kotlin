package com.netizen.eduman


import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import org.json.JSONException
import java.util.*

class FragGetStudentListSelection : Fragment(), AdapterView.OnItemSelectedListener {

    private var btn_get_all_std_list: Button? = null
    private var btn_enroll_student: Button? = null
    private var btn_find_student: Button? = null

    private var spinner_section: Spinner? = null

    private var selectSection: String? = null
    // private var localSectionID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()

    private var jObjPost: String? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var v: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_list_selection_layout, container, false)
        initializeViews()
        //loadServerSectionData()

        setsectionSpinnerData()

        return v
    }

    private fun initializeViews() {
        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)
        // activity?.title = "STUDENT"
        //activity?.titleColor==R.color.colorEduman
        activity?.title = Html.fromHtml("<font color='#2F3292'>STUDENT</font>")

        btn_get_all_std_list = v?.findViewById<View>(R.id.btn_get_list) as Button
        btn_enroll_student = v?.findViewById<View>(R.id.btn_enroll_student) as Button
        btn_find_student = v?.findViewById<View>(R.id.btn_find_student) as Button

        btn_find_student?.setBackgroundColor(resources.getColor(R.color.colorEduman))
        btn_find_student?.setTextColor(resources.getColor(R.color.white))

        btn_enroll_student?.setBackgroundColor(Color.WHITE)
        btn_enroll_student?.setTextColor(Color.BLUE)

        btn_enroll_student?.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_blue, 0);
        btn_find_student?.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search_white, 0);

        spinner_section = v?.findViewById<View>(R.id.spinner_section) as Spinner
        spinner_section?.onItemSelectedListener = this

        btn_get_all_std_list?.setOnClickListener {
            try {

                val senrollmentFragment = FragmentStudentList()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_enroll_student?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentSEnrollmentForm()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

/*        btn_find_student?.setOnClickListener {
            try {
                val allstdListFragment = FragGetStudentListSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }*/
    }


    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in attendances?.indices!!) {
                attendances.get(i).getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }

    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_section) {
                selectSection = spinner_section!!.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    selectSection = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localSectionID = da.GetSectionID(selectSection!!)
                    da.close()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    private fun loadServerSectionData() {

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_SECTION + "(classConfigId, classShiftSection, instituteId) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(
                                    c.getString("classConfigId"),
                                    c.getString("classShiftSection"),
                                    c.getString("instituteId")
                                )
                            )

                            if (!sectionsArrayList.isEmpty()) {
                                // setsectionSpinnerData(sectionsArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error -> Log.d("loadServerSectionData", error.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        private val TAG = "FragmentAttendanceSummary"
        var localSectionID = ""
    }
}
