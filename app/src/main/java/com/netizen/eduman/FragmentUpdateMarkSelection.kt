package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.Group
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.Subject
import com.netizen.eduman.network.CloudRequest
import org.json.JSONException
import java.util.*

class FragmentUpdateMarkSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_input_mark_search: Button? = null

    private var spinner_section: Spinner? = null
    private var spinner_group: Spinner? = null
    private var spinner_exam: Spinner? = null
    private var spinner_subject: Spinner? = null

    private var selectSection: String? = null
    private var selectGroup: String? = null
    private var selectExam: String? = null
    private var selectSubject: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()
    internal var examArrayList = ArrayList<Exam>()
    internal var subjectsArrayList = ArrayList<Subject>()

    private var txt_back: TextView? = null
    private var input_title: TextView? = null


    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.input_mark_spinner_selection, container, false)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "input_mark_selection")
        editor.apply()

        initializeViews()

        return v
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onStart() {
        super.onStart()
        setsectionSpinnerData()
    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_input_mark_search = v?.findViewById<View>(R.id.btn_mark_in_search) as Button

        spinner_section = v?.findViewById<View>(R.id.spinner_mark_in_sec) as Spinner
        spinner_section?.onItemSelectedListener = this

        spinner_group = v?.findViewById<View>(R.id.spinner_mark_in_grp) as Spinner
        spinner_group?.onItemSelectedListener = this

        spinner_exam = v?.findViewById<View>(R.id.spinner_mark_in_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        spinner_subject = v?.findViewById<View>(R.id.spinner_mark_in_sub) as Spinner
        spinner_subject?.onItemSelectedListener = this


        btn_input_mark_search?.setOnClickListener {
            try {
                updatMarkSearch()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView
        input_title = v?.findViewById<View>(R.id.inputmark_title) as TextView
        input_title!!.text = "U p d a t e  M a r k"

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_mark_in_sec) {
                selectSection = spinner_section?.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    selectSection = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localSectionID = da.GetSectionID(selectSection!!)
                    loadServerGroupData()
                    loadServerExamData()
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_mark_in_grp) {
                selectGroup = spinner_group?.selectedItem.toString()
                if (selectGroup == resources.getString(R.string.select_group)) {
                    selectGroup = ""
                } else {
                    selectGroup = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localGroupID = da.GetGroupID(selectGroup!!)
                    loadServerSubjectData()
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_mark_in_exam) {
                selectExam = spinner_exam?.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    selectExam = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localExamID = da.GetExamID(selectExam!!)
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_mark_in_sub) {
                selectSubject = spinner_subject?.selectedItem.toString()
                if (selectSubject == resources.getString(R.string.select_subject)) {
                    selectSubject = ""
                } else {
                    selectSubject = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localSubjectID = da.GetSubjectID(selectSubject!!)
                    da.close()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Input mark", "Selected Spinner data")
        }

    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val inputMark = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section_input_mark))
            for (i in inputMark?.indices!!) {
                inputMark[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(
                Objects.requireNonNull<FragmentActivity>(activity),
                android.R.layout.simple_spinner_item,
                section
            )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Input mark", "Section spinner data")
        }
    }

    private fun setgroupSpinnerData(groupArrayList: ArrayList<Group>) {
        try {

            val group = ArrayList<String>()
            group.add(resources.getString(R.string.select_group))
            for (i in groupArrayList.indices) {
                groupArrayList[i].getGroupName()?.let { group.add(it) }
            }

            val groupAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, group)
            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_group?.adapter = groupAdapter
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Input mark", "Group spinner data")
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter =
                ArrayAdapter(
                    Objects.requireNonNull<FragmentActivity>(activity),
                    android.R.layout.simple_spinner_item,
                    exam
                )
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Input mark", "Exam spinner data")
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setSubjectSpinnerData(subjectsArrayList: ArrayList<Subject>) {
        try {

            val subject = ArrayList<String>()
            subject.add(resources.getString(R.string.select_subject))
            for (i in subjectsArrayList.indices) {
                subjectsArrayList[i].getSubjectName()?.let { subject.add(it) }
            }

            val subjectAdapter = ArrayAdapter(
                Objects.requireNonNull<FragmentActivity>(activity),
                android.R.layout.simple_spinner_item,
                subject
            )
            subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_subject?.adapter = subjectAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Input mark", "Subject spinner data")
        }
    }

    private fun updatMarkSearch() {
        if (!validateInputSection()) {
            return
        }

        if (!validateInputGroup()) {
            return
        }

        if (!validateInputExam()) {
            return
        }
        if (!validateInputSubject()) {
            return
        }

        val senrollmentFragment = FragmentUpdateMark()
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
        fragmentTransaction.commit()
    }

    private fun validateInputSec(): Boolean {
        if (selectSection?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Academic Year", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateInputSection(): Boolean {
        if (selectSection?.trim { it <= ' ' }!!.isEmpty()) {
            activity?.let { it1 ->
                AppController.instance?.Alert(
                    it1,
                    R.drawable.requred_alert,
                    "Failure",
                    "Please Fill up the required fields."
                )
            }
            return false
        }
        return true
    }

    private fun validateInputGroup(): Boolean {
        if (selectGroup?.trim { it <= ' ' }!!.isEmpty()) {
            getActivity()?.let { it1 ->
                AppController.instance?.Alert(
                    it1,
                    R.drawable.requred_alert,
                    "Failure",
                    "Please Fill up the required fields."
                )
            }
            return false
        }
        return true
    }

    private fun validateInputExam(): Boolean {
        if (selectExam?.trim { it <= ' ' }!!.isEmpty()) {
            getActivity()?.let { it1 ->
                AppController.instance?.Alert(
                    it1,
                    R.drawable.requred_alert,
                    "Failure",
                    "Please Fill up the required fields."
                )
            }
            return false
        }
        return true
    }

    private fun validateInputSubject(): Boolean {
        if (selectSubject?.trim { it <= ' ' }!!.isEmpty()) {
            getActivity()?.let { it1 ->
                AppController.instance?.Alert(
                    it1,
                    R.drawable.requred_alert,
                    "Failure",
                    "Please Fill up the required fields."
                )
            }

            return false
        }
        return true
    }


    private fun loadServerGroupData() {

        val hitURL =
            AppController.BaseUrl + "core/setting/group-configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID!!

        val jsonObjectRequest = object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        groupsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val obj = c.getJSONObject("classObject")

                            val grouplist = Group()
                            grouplist.setGroupId(obj.getString("id"))
                            grouplist.setGroupName(obj.getString("name"))

                            groupsArrayList.add(grouplist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_GROUP + "(GroupId, GroupName) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    obj.getString("id"),
                                    obj.getString("name")
                                )
                            )

                            if (!groupsArrayList.isEmpty()) {
                                setgroupSpinnerData(groupsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Input mark: Group", e.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerExamData() {
        val hitURL =
            AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID!!

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM + "(ExamId, ExamName) " +
                                        "VALUES(?, ?)",
                                arrayOf(jobj.getString("id"), jobj.getString("name"))
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Input mark: Exam", e.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerSubjectData() {
        val hitURL =
            AppController.BaseUrl + "core/setting/subjectconfiguration/list/by/class-config-id/and/group-id?access_token=" + accessToken + "&classConfigId=114102&groupId=3016222105"

        //val hitURL = AppController.BaseUrl + "core/setting/subjectconfiguration/list/by/class-config-id/and/group-id?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&groupId=" + localGroupID


        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("subject")
                            val subjectList = Subject()
                            subjectList.setSubjectId(jobj.getString("id"))
                            subjectList.setSubjectName(jobj.getString("name"))

                            subjectsArrayList.add(subjectList)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_INPUT_MARK_SUB + "(SubjectId, SubjectName) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    jobj.getString("id"),
                                    jobj.getString("name")
                                )
                            )

                            if (!subjectsArrayList.isEmpty()) {
                                setSubjectSpinnerData(subjectsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Input mark: Subject", e.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    companion object {
        private val TAG = "FragmentAttendanceSummary"
        var localSectionID = ""
        var localPeriodID = ""
        var localGroupID=""
        var localExamID=""
        var localSubjectID=""

    }
}
