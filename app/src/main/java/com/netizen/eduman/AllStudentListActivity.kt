/*
package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.Adapter.StudentListAdapter
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.Student
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.ArrayList
import java.util.HashMap


class AllStudentListActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var spinner_get_all_std_list: Spinner? = null
    private var selectSection_get_all_std_list: String? = null
    private val classConfigId: String? = null
    internal var sectionsArrayList_all_std = ArrayList<Section>()

    private var recyclerView: RecyclerView? = null
    private var adapter: StudentListAdapter? = null
    private var studentsArrayList: ArrayList<Student>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private val mContext: Context? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_recyler_list)
        initializeViews()
        loadServerSectionData()
        recyclerViewInit()
    }

    private fun initializeViews() {
        spinner_get_all_std_list = findViewById(R.id.select_section_get_all_std_list) as Spinner
        spinner_get_all_std_list!!.onItemSelectedListener = this
    }

    */
/*
   recycler view initialization method
    *//*

    fun recyclerViewInit() {
        // Initialize item list
        studentsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById(R.id.all_student_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = StudentListAdapter(studentsArrayList, this)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(this)
        // Set layout manager to position the items
        recyclerView!!.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView!!.adapter = adapter
    }

    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>) {
        val section = ArrayList<String>()
        section.add(resources.getString(R.string.select_section))
        for (i in sectionArrayList.indices) {
            section.add(sectionArrayList[i].getSectionName().toString())
            sectionArrayList[i].getSectionId().toString()
        }

        val sectionAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, section)
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_get_all_std_list!!.adapter = sectionAdapter
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val spinner = parent as Spinner

        if (spinner.id == R.id.select_section_get_all_std_list) {
            selectSection_get_all_std_list = spinner_get_all_std_list!!.selectedItem.toString()
            if (selectSection_get_all_std_list == resources.getString(R.string.select_section)) {
                selectSection_get_all_std_list = ""
            } else {
                studentsArrayList!!.clear()
                selectSection_get_all_std_list = parent.getItemAtPosition(position).toString()
                GetAllStudentDataWebService()
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    fun GetAllStudentDataWebService() {
        val hitURL =
            "http://192.168.31.120:8081/student/list/by/class-config-id?classConfigId=$selectSection_get_all_std_list&access_token=7e1bb8bc-c8ef-4821-8811-97a1eef7135c"

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val studentlist = Student()
                        //set the json data in the model
                        studentlist.setStudent_id(c.getString("studentId"))
                        studentlist.setStudent_roll(c.getString("studentRoll"))
                        studentlist.setStudent_name(c.getString("studentName"))
                        studentlist.setGroup(c.getString("groupName"))
                        studentlist.setCategory(c.getString("studentCategoryName"))
                        studentlist.setGender(c.getString("studentGender"))
                        studentlist.setReligion(c.getString("studentReligion"))
                        studentlist.setFather_name(c.getString("fatherName"))
                        studentlist.setMother_name(c.getString("motherName"))
                        studentlist.setMobile_no(c.getString("guardianMobile"))
                        //studentlist.setStudent_image(c.getString("image"))

                        // adding item to ITEM list
                        studentsArrayList?.add(studentlist)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                //Notify adapter if data change
                adapter?.notifyDataSetChanged()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            */
/**
             * Passing some request headers
             *//*

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    private fun loadServerSectionData() {

        val hitURL = "https://api.netizendev.com:2083/emapi/core/setting/class-configuration/list?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc"

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList_all_std = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))
                            sectionsArrayList_all_std.add(sectionlist)
                            if (!sectionsArrayList_all_std.isEmpty()) {
                                setsectionSpinnerData(sectionsArrayList_all_std)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@AllStudentListActivity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(
                        this@AllStudentListActivity,
                        error.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    companion object {
        private val TAG = "AllStudentListActivity"
    }
}
*/
