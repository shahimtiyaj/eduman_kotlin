package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_HR_DESIG
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Designation
import com.netizen.eduman.model.Group
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.util.ArrayList
import java.util.HashMap

class FragmentHRnrollmentForm : Fragment(), AdapterView.OnItemSelectedListener {
    private var btn_reg_hr: Button? = null
    private var btn_enroll_hr: Button? = null
    private var btn_find_hr_list: Button? = null

    private var spinner_category_hr: Spinner? = null
    private var spinner_designation_hr: Spinner? = null
    private var spinner_hr_id: Spinner? = null
    private var spinner_gender_hr: Spinner? = null
    private var spinner_religion_hr: Spinner? = null

    private var inputHRCustomID: EditText? = null
    private var inputHRName: EditText? = null
    private var inputHRPhone: EditText? = null
    private var custom_id_hr_head: TextView? = null


    private var hrCustom: String? = null
    private var hrname: String? = null
    private var phone_hr: String? = null

    private var selectCategory_hr: String? = null
    private var selectIdType_hr: String? = null
    private var selectGender_hr: String? = null
    private var selectReligion_hr: String? = null
    private var selectDesignation_hr: String? = null
    private var id_type_hr: String? = null
    private var localDesignationID: String? = null

    private var inputLayoutCustomID_hr: TextInputLayout? = null
    private var inputLayoutHRName: TextInputLayout? = null
    private var inputLayouHRMobile: TextInputLayout? = null

    private val toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    internal var academicYearsArrayList = ArrayList<AcademicYear>()
    internal var designationArrayList = ArrayList<Designation>()
    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()

    private var jsonObjectPost: String? = null

    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_enlistment_layout, container, false)
        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_reg")
        editor.apply()
        return v
    }

    override fun onStart() {
        super.onStart()
        loadServerHRDesignationData()
    }

    private fun initializeViews() {

        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        activity?.setTitle(Html.fromHtml("<font color='#2F3292'>HR Enrollment</font>"))


        inputHRCustomID = v?.findViewById<View>(R.id.et_hr_custom_id) as EditText
        inputHRName = v?.findViewById<View>(R.id.et_hr_name) as EditText
        inputHRPhone = v?.findViewById<View>(R.id.et_hr_mobile) as EditText

        inputLayoutHRName = v?.findViewById<View>(R.id.input_layout_hr_name) as TextInputLayout
        inputLayoutCustomID_hr = v?.findViewById<View>(R.id.input_layout_hr_custom_id) as TextInputLayout
        inputLayouHRMobile = v?.findViewById<View>(R.id.input_layout_mobile_hr) as TextInputLayout
        custom_id_hr_head = v?.findViewById<View>(R.id.hr_head) as TextView


        inputHRCustomID?.addTextChangedListener(MyTextWatcher(inputHRCustomID!!))
        inputHRName?.addTextChangedListener(MyTextWatcher(inputHRName!!))
        inputHRPhone?.addTextChangedListener(MyTextWatcher(inputHRPhone!!))

        btn_reg_hr = v?.findViewById<View>(R.id.btn_hr_reg_submit) as Button
        btn_find_hr_list = v?.findViewById<View>(R.id.btn_find_hr) as Button

        //  toolbarTitle = v?.findViewById<View>(R.id.toolbarTitle) as TextView
        //  toolbarTitle?.text = "HR Enlistment Form"

        spinner_gender_hr = v?.findViewById<View>(R.id.spinner_gender_hr) as Spinner
        spinner_gender_hr?.onItemSelectedListener = this

        spinner_religion_hr = v?.findViewById<View>(R.id.spinner_religion_hr) as Spinner
        spinner_religion_hr?.onItemSelectedListener = this

        spinner_category_hr = v?.findViewById<View>(R.id.spinner_category_hr) as Spinner
        spinner_category_hr?.onItemSelectedListener = this

        spinner_designation_hr = v?.findViewById<View>(R.id.spinner_designation_hr) as Spinner
        spinner_designation_hr?.onItemSelectedListener = this

        spinner_hr_id = v?.findViewById<View>(R.id.spinner_hr_id_type) as Spinner
        spinner_hr_id?.onItemSelectedListener = this

        btn_reg_hr?.setOnClickListener {
            try {
                submitRegistration()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_find_hr_list?.setOnClickListener {
            try {
                val allstdListFragment = FragmentHRList()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitRegistration() {
        if (!validateName()) {
            return
        }

        if (!validateHRGender()) {
            return
        }
        if (!validateHRReligion()) {
            return
        }

        if (!validateHRCategory()) {
            return
        }

        if (!validateHRDesignation()) {
            return
        }

        if (!validatePhone()) {
            return
        }

        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        hrCustom = inputHRCustomID!!.text.toString()
        hrname = inputHRName!!.text.toString()
        phone_hr = inputHRPhone!!.text.toString()

        dataSendToServer()
    }

    fun dataSendToServer() {
        val hitURL =
            AppController.BaseUrl + "staff/basic/save?registrationType=" + selectIdType_hr + "&access_token=" + accessToken
        activity?.let { AppController.instance?.showProgress(it, "Wait", "Sending data to the server") }

        //Json object for posting to server and getting object array
        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {

            arrayItem.put("category", selectCategory_hr)
            arrayItem.put("customId", hrCustom)
            arrayItem.put("designationId", localDesignationID)
            arrayItem.put("gender", selectGender_hr)
            arrayItem.put("staffMobile1", phone_hr)
            arrayItem.put("staffName", hrname)
            arrayItem.put("staffReligion", selectReligion_hr)

            jsonArray.put(arrayItem)

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        //here convert json object to string for posting data to server
        jsonObjectPost = jsonArray.toString()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        Toast.makeText(activity, status, Toast.LENGTH_LONG).show()
                        activity?.let { AppController.instance?.hideProgress(it) }

                    } else {
                        Toast.makeText(activity, "Registration fail!!", Toast.LENGTH_LONG).show()
                        activity?.let { AppController.instance?.hideProgress(it) }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }


                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {

            /**
             * Passing some request in body. Basically here we have passed json object request
             */
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jsonObjectPost == null) null else jsonObjectPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jsonObjectPost,
                        "utf-8"
                    )
                    return null
                }
            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }
        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun loadServerHRDesignationData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2601"
        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }


        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        designationArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val designationlist = Designation()
                            designationlist.setDesignationId(c.getString("id"))
                            designationlist.setDesignationName(c.getString("name"))

                            designationArrayList.add(designationlist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_HR_DESIG + "(DesigId, DesigName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("id"), c.getString("name"))
                            )

                            if (!designationArrayList.isEmpty()) {
                                setdesignationSpinnerData(designationArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        activity?.let { AppController.instance?.hideProgress(it) }
                    }
                    activity?.let { AppController.instance?.hideProgress(it) }

                }, Response.ErrorListener {
                    // Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();

                    Log.d("loadServerHRCategory", "Server error")
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["typeId"] = "2601"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun setdesignationSpinnerData(designationArrayList: ArrayList<Designation>) {

        val hrdesignation = ArrayList<String>()

        hrdesignation.add(resources.getString(R.string.select_designation))

        for (i in designationArrayList.indices) {
            designationArrayList[i].getDesignationName()?.let { hrdesignation.add(it) }
        }

        val categoryAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, hrdesignation)
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_designation_hr?.adapter = categoryAdapter
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_category_hr) {
                selectCategory_hr = spinner_category_hr!!.selectedItem.toString()
                if (selectCategory_hr == resources.getString(R.string.select_category)) {
                    selectCategory_hr = ""
                } else {
                    selectCategory_hr = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_designation_hr) {
                selectDesignation_hr = spinner_designation_hr!!.selectedItem.toString()
                if (selectDesignation_hr == resources.getString(R.string.select_designation)) {
                    selectDesignation_hr = ""
                } else {
                    selectDesignation_hr = parent.getItemAtPosition(position).toString()
                    val da = DAO(activity!!)
                    da.open()
                    localDesignationID = da.GetDesignationID(selectDesignation_hr!!)
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_gender_hr) {
                selectGender_hr = spinner_gender_hr!!.selectedItem.toString()
                if (selectGender_hr == resources.getString(R.string.select_gender)) {
                    selectGender_hr = ""
                } else {
                    selectGender_hr = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_religion_hr) {
                selectReligion_hr = spinner_religion_hr!!.selectedItem.toString()
                if (selectReligion_hr == resources.getString(R.string.select_religion)) {
                    selectReligion_hr = ""
                } else {
                    selectReligion_hr = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_hr_id_type) {
                selectIdType_hr = spinner_hr_id!!.selectedItem.toString()
                if (selectIdType_hr == resources.getString(R.string.select_student_id_type)) {
                    selectIdType_hr = ""
                } else {
                    selectIdType_hr = parent.getItemAtPosition(position).toString()
                    if (selectIdType_hr == "Custom") {
                        id_type_hr = 1.toString()
                        inputHRCustomID?.visibility = View.VISIBLE
                        custom_id_hr_head?.visibility = View.VISIBLE


                    } else {
                        id_type_hr = 0.toString()
                        inputHRCustomID?.visibility = View.GONE
                        custom_id_hr_head?.visibility = View.GONE
                    }

                }
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("HR reg  ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et_hr_name -> validateName()
                R.id.et_hr_mobile -> validatePhone()
            }
        }
    }


    private fun validatePhone(): Boolean {
        if (inputHRPhone?.getText().toString().trim { it <= ' ' }.isEmpty() || inputHRPhone?.length() != 11) {
            inputLayouHRMobile?.setError(getString(R.string.err_msg_phone))
            inputHRPhone?.let { requestFocus(it) }
            return false
        } else {
            inputLayouHRMobile?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateName(): Boolean {
        if (inputHRName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputHRName?.setError(getString(R.string.err_mssg_name))
            inputHRName?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutHRName?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateTypeID(): Boolean {
        if (inputHRCustomID?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputHRCustomID?.setError(getString(R.string.err_mssg_id))
            inputHRCustomID?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutCustomID_hr?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateHRGender(): Boolean {
        if (selectGender_hr?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Gender", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateHRReligion(): Boolean {
        if (selectReligion_hr?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Religion", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateHRCategory(): Boolean {
        if (selectCategory_hr?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Category", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateHRDesignation(): Boolean {
        if (selectDesignation_hr?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Designation", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    companion object {

        private val TAG = "FragmentHRnrollmentForm"
    }
}
