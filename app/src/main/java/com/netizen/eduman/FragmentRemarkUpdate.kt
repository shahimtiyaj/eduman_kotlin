package com.netizen.eduman

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.Adapter.RemarkUpdateAdapter
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_STUDENT_RESULT
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class FragmentRemarkUpdate : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_remarks_update_search: Button? = null
    private var btn_remarks_update: Button? = null
    private var spinner_section: Spinner? = null
    private var spinner_exam: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null
    private val selectRemarks: String? = null
    private val remarksDescription: String? = null
    private val localPeriodID: String? = null
    private var localSectionID: String? = null
    private var localExamID: String? = null
    private var localRemarkID: String? = null


    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    private var jObjPost: String? = null
    lateinit var v: View

    private var recyclerView: RecyclerView? = null
    private var adapter: RemarkUpdateAdapter? = null
    private var remarksSummaryArrayList: ArrayList<ResultSummary>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private val inputRemarksDescription: EditText? = null
    private val inputLayoutRemarksDescription: TextInputLayout? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.remarks_update_recyler_list_view, container, false)
        initializeViews()
        recyclerViewInit()
        loadServerSectionData()
        return v
    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_remarks_update_search = v.findViewById<View>(R.id.btn_all_remarks_update_search) as Button
        btn_remarks_update = v.findViewById<View>(R.id.btn_remarks_all_update) as Button

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        total_found = v.findViewById<View>(R.id.total_remarks_update_found) as TextView
        total_found?.text = dao?.totalRemarksUpdateStudent().toString()
        dao?.close()

        spinner_section = v.findViewById<View>(R.id.spinner_remarks_update_section) as Spinner
        spinner_section?.onItemSelectedListener = this

        spinner_exam = v.findViewById<View>(R.id.spinner_remarks_update_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        btn_remarks_update_search?.setOnClickListener {
            try {

                if (localExamID == null || localSectionID == null) {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.requred_alert,
                            "Failure",
                            "Please Fill up the required fields."
                        )
                    }
                } else {
                    GetStudentResultSummary()
                    loadAllremarksListUpdate()
                }



            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_remarks_update?.setOnClickListener {
            try {

                if (remarksSummaryArrayList?.isNotEmpty()!!) {
                    remarksdataSendToServer()
                } else {
                    getActivity()?.let { it1 ->
                        AppController.instance?.Alert(
                            it1,
                            R.drawable.fail,
                            "Failure",
                            "Sorry ! Empty list."
                        )
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        remarksSummaryArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.all_remarks_update_recyler_view_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = RemarkUpdateAdapter(activity!!, remarksSummaryArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    private fun loadAllremarksListUpdate() {
        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        remarksSummaryArrayList = dao?.getAllRemarksStudent()
        if (!remarksSummaryArrayList?.isEmpty()!!) {
            adapter = RemarkUpdateAdapter(context!!, remarksSummaryArrayList!!)
            recyclerView!!.setHasFixedSize(true)
            val mLayoutManager = LinearLayoutManager(context)
            recyclerView!!.layoutManager = mLayoutManager
            recyclerView!!.adapter = adapter
            adapter!!.notifyDataSetChanged()
        }
        dao?.close()
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

        val spinner = parent as Spinner
        if (spinner.id == R.id.spinner_remarks_update_section) {
            selectSection = spinner_section?.selectedItem.toString()
            if (selectSection == resources.getString(R.string.select_section)) {
                selectSection = ""
            } else {
                selectSection = parent.getItemAtPosition(position).toString()
                val da = DAO(activity!!)
                da.open()
                localSectionID = da.GetSectionID(selectSection!!)
                loadServerExamData()
                da.close()
            }
        }
        if (spinner.id == R.id.spinner_remarks_update_exam) {
            selectExam = spinner_exam?.selectedItem.toString()
            if (selectExam == resources.getString(R.string.select_exam)) {
                selectExam = ""
            } else {
                selectExam = parent.getItemAtPosition(position).toString()

                val da = DAO(activity!!)
                da.open()
                localExamID = da.GetExamID(selectExam!!)
                da.close()
            }
        }

    }
    catch (e: Exception) {
        e.printStackTrace()
    }
    catch (e: IllegalStateException) {
        e.printStackTrace()
    } catch (e: NullPointerException) {
        e.printStackTrace()
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }

    finally {
        Log.d("Remark Update ", "Spinner data")
    }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>) {
        try {

        val section = ArrayList<String>()
        section.add(resources.getString(R.string.select_section))
        for (i in sectionArrayList.indices) {
            sectionArrayList[i].getSectionName()?.let { section.add(it) }
        }

        val sectionAdapter = ArrayAdapter(
            Objects.requireNonNull<FragmentActivity>(activity),
            android.R.layout.simple_spinner_item,
            section
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_section!!.adapter = sectionAdapter

        }
        catch (e: Exception) {
            e.printStackTrace()
        }
        catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
        finally {
            Log.d("Remark update ", "Section spinner data")
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

        val exam = ArrayList<String>()
        exam.add(resources.getString(R.string.select_exam))
        for (i in examArrayList.indices) {
            examArrayList[i].getExamName()?.let { exam.add(it) }
        }

        val examAdapter =
            ArrayAdapter(Objects.requireNonNull<FragmentActivity>(activity), android.R.layout.simple_spinner_item, exam)
        examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_exam!!.adapter = examAdapter

        }
        catch (e: Exception) {
            e.printStackTrace()
        }
        catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
        finally {
            Log.d("Remark update exam ", " spinner data")
        }
    }

    fun GetStudentResultSummary() {

        //final String hitURL = AppController.BaseUrl + "exam/report/section-wise/result/details?access_token=c54ff645-c423-4300-9414-450a9e22cd5b&classConfigId=100175&examConfigId=62";
        val hitURL =
            AppController.BaseUrl + "exam/report/section-wise/result/details?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&examConfigId=" + localExamID

        activity?.let {
            AppController.instance?.showProgress(
                it,
                "Please Wait",
                "Loading data..."
            )
        }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + TABLE_STUDENT_RESULT + "(identificationId, customStudentId, studentRoll, " +
                                    "studentName, totalMarks, letterGrade, gradingPoint, remarkId, remarks, remarksTitle) " +
                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            arrayOf(
                                c.getString("identificationId"),
                                c.getString("customStudentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("totalMarks"),
                                c.getString("letterGrade"),
                                c.getString("gpa"),
                                c.getString("remarkId"),
                                c.getString("remarks"),
                                c.getString("remarksTitle")
                            )
                        )

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
        //activity?.let { AppController.instance?.hideProgress(it) }

    }

    private fun localRemarkUpdateList() {
        remarksSummaryArrayList = ArrayList()
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val resultSummaries = dao?.getAllRemarksStudentUpdate()

            val jsonObj = JSONObject()
            val jsonArray = JSONArray()

            for (resultSummary in resultSummaries!!) {

                jsonObj.put("examConfigId", localExamID)//62
                jsonObj.put("identificationId", resultSummary.result_std_identification)
                jsonObj.put("remarkId", resultSummary.result_std_remarks_id)
                jsonObj.put("remarks", resultSummary.result_std_remarks_des)
                jsonObj.put("remarksTitle", resultSummary.result_std_remarks_title)
                jsonArray.put(jsonObj)
            }

            jObjPost = jsonArray.toString()
            Log.d("Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun remarksdataSendToServer() {

      //  val hitURL = "http://192.168.31.14:8082/exam/remarks/all/update?access_token=c54ff645-c423-4300-9414-450a9e22cd5b"
        val hitURL = AppController.BaseUrl+"exam/remarks/all/update?access_token="+accessToken
        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Updating data...") }

        localRemarkUpdateList()

        val postRequest = object : CustomRequest(
            Request.Method.PUT, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Remarks update Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.success,
                                "Remarks Data",
                                "Remarks data successfully updated "
                            )
                        }
                        activity?.let { AppController.instance?.hideProgress(it) }
                    } else {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.fail,
                                "Remarks ",
                                "Opps!! fail to update"
                            )
                        }
                        activity?.let { AppController.instance?.hideProgress(it) }                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

            }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }


                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
        activity?.let { AppController.instance?.hideProgress(it) }

    }


    private fun loadServerSectionData() {
        val hitURL =
            AppController.BaseUrl+"core/setting/class-configuration/list?access_token="+accessToken
        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            DAO.executeSQL(
                                ("INSERT OR REPLACE INTO " + TABLE_SECTION + "(classConfigId, classShiftSection) " +
                                        "VALUES(?, ?)"),
                                arrayOf(c.getString("classConfigId"), c.getString("classShiftSection"))
                            )

                            if (!sectionsArrayList.isEmpty()) {
                                setsectionSpinnerData(sectionsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    private fun loadServerExamData() {
        val hitURL =
            AppController.BaseUrl+"exam/configuration/list/by/class-config-id?access_token="+accessToken+"&classConfigId=" + localSectionID!!

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM + "(ExamId, ExamName) " +
                                        "VALUES(?, ?)",
                                arrayOf(jobj.getString("id"), jobj.getString("name"))
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        private val TAG = "FragmentRemarkUpdate"
    }
}
