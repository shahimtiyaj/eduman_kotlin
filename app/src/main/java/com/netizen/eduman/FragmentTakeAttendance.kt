package com.netizen.eduman

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.AttendanceAdapter
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.dateOfAttendance
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.localPeriodID
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_TAKEATTENDANCE
import com.netizen.eduman.model.Attendance
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.jetbrains.annotations.Nullable
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*


class FragmentTakeAttendance : Fragment() {
    private var btn_serach_all: Button? = null
    private var btn_attendance_save: Button? = null
    private var btn_take_student_atd: Button? = null
    private var btn_student_atd_list: Button? = null
    private var btn_hr_atd_list: Button? = null
    private var txt_back: TextView? = null


    private var et_attendance_search_signle: EditText? = null

    // private var localPeriodID: String? = null
    //  private var dateOfAttendance: String? = null
    // private var localSectionID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()

    private var v: View? = null

    private var recyclerView: RecyclerView? = null
    private var adapter: AttendanceAdapter? = null
    private var attendancessArrayList: ArrayList<Attendance>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private var jObjPost: String? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(@Nullable inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.take_attendance_layout_list, container, false)

        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_take_atd")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()

        try {
            loadMessageList()
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }

    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        et_attendance_search_signle = v?.findViewById<View>(R.id.et_attendance_search_signle) as EditText

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        total_found = v?.findViewById<View>(R.id.total_found) as TextView
        total_found?.text = dao?.totalAttendanceStudentCount().toString()
        dao?.close()

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        btn_take_student_atd = v?.findViewById<View>(R.id.btn_take_student_atd) as Button
        btn_student_atd_list = v?.findViewById<View>(R.id.btn_student_atd_list) as Button
        btn_hr_atd_list = v?.findViewById<View>(R.id.btn_hr_atd_list) as Button

        btn_attendance_save = v?.findViewById<View>(R.id.btn_attendance_save) as Button

        btn_student_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentSAttendanceSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_hr_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = TabFragmentHPAL()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_attendance_save?.setOnClickListener {
            try {
                attendancedataSendToServer()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        et_attendance_search_signle?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        attendancessArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_attendance_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = AttendanceAdapter(activity!!, attendancessArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }


    fun attendancedataSendToServer() {

        val hitURL = AppController.mainUrl + "manual/attendance/save/for/student?access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Sending attendance data..") }

        sendAttendanceList()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.success,
                                "Attendance",
                                "Attendance data successfully sent"
                            )
                        }
                        activity?.let { AppController.instance?.hideProgress(it) }
                    } else {
                        activity?.let { AppController.instance?.Alert(it, R.drawable.fail, "Failure", status) }
                        activity?.let { AppController.instance?.hideProgress(it) }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }
            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun loadMessageList() {

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        attendancessArrayList = dao?.allTakeAttendance
        if (!attendancessArrayList?.isEmpty()!!) {
            activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

            adapter = AttendanceAdapter(context!!, attendancessArrayList!!)
            recyclerView?.setHasFixedSize(true)
            val mLayoutManager = LinearLayoutManager(context)
            recyclerView?.layoutManager = mLayoutManager
            recyclerView?.adapter = adapter
            adapter?.notifyDataSetChanged()
        }

        if (attendancessArrayList?.isEmpty()!!) {
            AppController.instance?.Alert(activity!!, R.drawable.not_found, "Failure", "Sorry no data found.")
            activity?.let { AppController.instance?.hideProgress(it) }
        }

        dao?.close()
        activity?.let { AppController.instance?.hideProgress(it) }
    }

    /*  class backgroundTask() : AsyncTask<Void, Void, String>() {

          override fun onPreExecute() {
              super.onPreExecute()
          }

          override fun doInBackground(vararg params: Void?): String? {
              loadMessageList()
              return null
          }

          override fun onPostExecute(result: String?) {
              super.onPostExecute(result)
          }
      }*/


    private fun sendAttendanceList() {
        attendancessArrayList = ArrayList()

        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allTakeAttendance

            val jsonObj = JSONObject()
            val jsonObj1 = JSONObject()

            val jsonArray = JSONArray()
            val jsonArray1 = JSONArray()

            if (attendances != null) {
                for (attend in attendances) {
                    val arrayItem = JSONObject()

                    arrayItem.put("identificationId", attend.getstudent_identification())
                    arrayItem.put("checked", attend.getStudent_absent())

                    jsonArray.put(arrayItem)
                }
            }
            try {
                jsonObj.put("identificationRqHelpers", jsonArray)
                jsonArray1.put(jsonObj)

                jsonObj1.put("attendanceDate", dateOfAttendance)//"01/01/2019"
                jsonObj1.put("classConfigId", localSectionID)
                jsonObj1.put("periodId", localPeriodID) //"922102301"
                jsonObj1.put("periodRequests", jsonArray1)

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            jObjPost = jsonObj1.toString()
            Log.d("Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun GetAllStudentAttendanceWebService() {

        val mainFormat = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        val parsedDate = mainFormat.parse(dateOfAttendance)
        val formatGet = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val date = formatGet.format(parsedDate)

        val hitURL =
            AppController.mainUrl + "student/list/by/class-config-id?access_token=" + accessToken + "&attendanceDate=$date&classConfigId=$localSectionID&periodId=$localPeriodID"

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    val getData = response.getJSONArray("item")
                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_TAKEATTENDANCE + "(studentId, studentRoll, studentName, studentGender, identificationId) " +
                                    "VALUES(?, ?, ?, ?, ?)"),
                            arrayOf(
                                c.getString("studentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("studentGender"),
                                c.getString("identificationId")
                            )
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

                if (attendancessArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(activity!!, R.drawable.not_found, "Failure", "Sorry no data found.")
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                total_found?.text = dao?.totalAbssentStudent().toString()
                dao?.close()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }

    companion object {
        private val TAG = "FragmentTakeAttendance"
    }
}
