package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.ResultSummaryAdapter
import com.netizen.eduman.FragmentStudentResultSelection.Companion.localExamID
import com.netizen.eduman.FragmentStudentResultSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.*

class FragmentStudentResult : Fragment() {

    private var result_student_id_search: EditText? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    lateinit var v: View

    private var recyclerView: RecyclerView? = null
    private var adapter: ResultSummaryAdapter? = null
    private var resultSummaryArrayList: ArrayList<ResultSummary>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private var txt_back: TextView? = null


    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    internal lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.result_summary_recyler_list_view, container, false)
        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_result_details")
        editor.apply()


        return v
    }

    override fun onStart() {
        super.onStart()
        GetStudentResultSummary()
    }

    private fun initializeViews() {

        result_student_id_search = v.findViewById<View>(R.id.et_result_student_id_search) as EditText

        total_found = v.findViewById<View>(R.id.total_found) as TextView

        sharedpreferences = activity!!.getSharedPreferences(
            MyPREFERENCES, 0
        )
        accessToken = sharedpreferences.getString("accessToken", null)

        result_student_id_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentStudentResultSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        resultSummaryArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.all_result_summary_recyler_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = ResultSummaryAdapter(activity!!, resultSummaryArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }


    fun GetStudentResultSummary() {
        val hitURL =
            AppController.BaseUrl + "exam/report/section-wise/result/details?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&examConfigId=" + localExamID
        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)


                        val resultSummary = ResultSummary()
                        resultSummary.result_std_id = c.getString("studentId")
                        resultSummary.result_std_roll = c.getString("studentRoll")
                        resultSummary.result_std_name = c.getString("studentName")
                        resultSummary.result_std_total_mark = c.getString("totalMarks")
                        resultSummary.result_std_grade = c.getString("letterGrade")
                        resultSummary.result_std_gpa = c.getString("gradingPoint")
                        resultSummary.result_std_status = c.getString("passFailStatus")
                        resultSummary.result_std_sec_merit = c.getString("sectionPosition")
                        resultSummary.result_std_no_failed = c.getString("numOfFailedSubjects")

                        // adding item to ITEM list
                        resultSummaryArrayList?.add(resultSummary)

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

                if (resultSummaryArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(activity!!, R.drawable.not_found, "Failure", "Sorry no data found.")
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                total_found?.setText(resultSummaryArrayList?.size.toString())
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
        // activity?.let { AppController.instance?.hideProgress(it) }

    }

    companion object {

        private val TAG = "FragmentStudentResult"
    }

}
