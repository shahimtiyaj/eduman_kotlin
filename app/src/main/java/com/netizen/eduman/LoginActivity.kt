package com.netizen.eduman

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import com.victor.loading.newton.NewtonCradleLoading
import org.json.JSONException
import java.util.*
import kotlin.collections.HashMap


class LoginActivity : AppCompatActivity() {


    private val TAG = LoginActivity::class.java.simpleName
    private var bookLoading: NewtonCradleLoading? = null
    internal var sectionsArrayList = ArrayList<Section>()
    internal var academicYearsArrayList = ArrayList<AcademicYear>()


    internal var input_userName: AppCompatEditText? = null
    internal var input_password: AppCompatEditText? = null
    internal var input_random_sum: EditText? = null
    internal var rememberMe: CheckBox? = null


    internal var inputLayoutUserName: TextInputLayout? = null
    internal var inputLayoutUserPass: TextInputLayout? = null

    internal var input_value1: TextView? = null
    internal var input_value3: TextView? = null

    private var btn_login: Button? = null

    internal var name: String? = null
    internal var pass: String? = null
    internal var sum: String? = null
    private val random_sum = null

    internal var grant_type = "password"

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFullScreenWindow()
        setContentView(R.layout.login_layout)
        initializeView()
    }

    private fun initializeView() {
        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()

//        bookLoading = findViewById<NewtonCradleLoading>(R.id.newton_cradle_loading) as NewtonCradleLoading
        input_userName = findViewById<AppCompatEditText>(R.id.input_userName) as AppCompatEditText
        input_password = findViewById<AppCompatEditText>(R.id.input_password) as AppCompatEditText
        input_random_sum = findViewById<EditText>(R.id.input_sum) as EditText
        rememberMe = findViewById<CheckBox>(R.id.remember_me_id) as CheckBox


        val userLogin = dao?.getUserDetails()
        if (userLogin?.getuserName().isNullOrBlank()) {
            rememberMe?.isChecked = false
        } else {
            rememberMe?.isChecked = true
            input_userName?.setText(userLogin?.getuserName())
            input_password?.setText(userLogin?.getuserPassword())
        }

        rememberMe?.setOnCheckedChangeListener { buttonView, isChecked ->
            try {
                if (isChecked) {
                    name = input_userName?.text.toString()
                    pass = input_password?.text.toString()
                    rememberMe?.isChecked = true

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_LOGIN + "(userName, password) " +
                                "VALUES(?, ?)", arrayOf(name!!, pass!!)
                    )

                } else {
                    dao?.deleteUsers()
                    input_userName!!.text?.clear()
                    input_password!!.text?.clear()
                }

            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }

        inputLayoutUserName = findViewById(R.id.input_layout_user) as TextInputLayout
        inputLayoutUserPass = findViewById(R.id.input_layout_pass) as TextInputLayout

        input_userName?.addTextChangedListener(MyTextWatcher(input_userName!!))
        input_password?.addTextChangedListener(MyTextWatcher(input_password!!))

        val r = Random()
        val randomNumber1 = r.nextInt(15)
        val randomNumber2 = Random().nextInt(10 - 5 + 1) + 5

        input_value1 = findViewById<TextView>(R.id.value1)
        input_value3 = findViewById<TextView>(R.id.value3)

        input_value1?.text = randomNumber1.toString()
        input_value3?.text = randomNumber2.toString()


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)

        btn_login = findViewById<Button>(R.id.btn_login)
        btn_login?.setOnClickListener {
            try {
                val inputRandomSum = Integer.decode(input_random_sum!!.getText().toString())
                val random_sum = randomNumber1 + randomNumber2

                if (input_random_sum?.getText()!!.toString().trim { it <= ' ' }.isEmpty() || !(inputRandomSum.equals(
                        random_sum
                    ))
                ) {
                    input_random_sum?.setError(getString(R.string.err_msg_randomsum))
                } else {
                    sentLoginInformation()
                }
            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
        }
    }


    private fun sentLoginInformation() {
        if (!validateName()) {
            return
        }
        if (!validatePassword()) {
            return
        }

        name = input_userName?.text.toString()
        pass = input_password?.text.toString()
        sum = input_random_sum?.text.toString()

        try {
            dataSendToServer()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.input_userName -> validateName()
                R.id.input_password -> validatePassword()
            }
        }
    }

    private fun validateName(): Boolean {
        if (input_userName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            input_userName?.setError(getString(R.string.err_msg_name))
            requestFocus(input_userName!!)
            return false
        } else {
            inputLayoutUserName?.setErrorEnabled(false)
        }

        return true
    }

    private fun validatePassword(): Boolean {
        if (input_password?.getText()!!.toString().trim { it <= ' ' }.isEmpty()) {
            input_password?.setError(getString(R.string.err_msg_pass))
            requestFocus(input_password!!)
            return false
        } else {
            inputLayoutUserPass?.setErrorEnabled(false)
        }

        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    fun dataSendToServer() {
        // bookLoading?.visibility = View.VISIBLE

        val hitURL = AppController.BaseUrl + "oauth/token"

        val params = HashMap<String?, String?>()

        params["client_id"] = "eduman-web-read-write-client" //Items - Item 1 - name
        params["grant_type"] = grant_type //Items - passport
        params["username"] = name //Items - Item 3 - phone
        params["password"] = pass //Items - Item 3 - catagory
        AppController.instance?.showProgress(this@LoginActivity, title = "Please Wait", message = "logging...")
        //bookLoading?.isStart()
        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, params,
            Response.Listener { response ->
                Log.d(TAG, "Server Response: $response")

                try {

                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))

                    val acesstoken = response.getString("access_token")
                    val tokentype = response.getString("token_type")
                    val refreshtoken = response.getString("refresh_token")

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_TOKEN + "(access_token, refresh_token, token_type) " +
                                "VALUES(?, ?, ?)", arrayOf(acesstoken, tokentype, refreshtoken)
                    )

                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
                    val editor = sharedpreferences.edit()
                    editor.putString("accessToken", acesstoken)
                    editor.putString("tokenType", tokentype)
                    editor.putString("refresh_token", refreshtoken)
                    editor.apply()
                    AppController.instance?.hideProgress(this@LoginActivity)

                    // bookLoading?.stop()
                    // bookLoading?.visibility = View.GONE

                } catch (e: JSONException) {
                    e.printStackTrace()
                    AppController.instance?.hideProgress(this@LoginActivity)
                    // bookLoading?.stop()
                    // bookLoading?.visibility = View.GONE
                }
                authorization()
                loadServerSectionData()
                loadServerAcademicYearData()
                AppController.instance?.hideProgress(this@LoginActivity)
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                //  AppController.instance?.hideProgress(this@LoginActivity)
                bookLoading?.stop()
                bookLoading?.visibility = View.GONE

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val client_Key =
                    "Basic ZWR1bWFuLXdlYi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=="
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Authorization"] = client_Key

                return headers
            }
        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    fun authorization() {
        val hitURL = AppController.BaseUrl + "institute/setting/user/info/details"
        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Server Response: $response")
                try {
                    val message = response.getString("message")
                    val msgtype = response.getString("msgType")

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {


            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                sharedpreferences = applicationContext.getSharedPreferences(MyPREFERENCES, 0)
                val h_acess_token = sharedpreferences.getString("accessToken", null)
                val h_token_type = sharedpreferences.getString("tokenType", null)

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Authorization"] = "$h_token_type $h_acess_token"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    private fun loadServerSectionData() {
        val accessToken = sharedpreferences.getString("accessToken", null)

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_SECTION + "(classConfigId, classShiftSection, instituteId) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(
                                    c.getString("classConfigId"),
                                    c.getString("classShiftSection"),
                                    c.getString("instituteId")
                                )
                            )

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error -> Log.d("loadServerSectionData", error.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    private fun loadServerAcademicYearData() {
        val accessToken = sharedpreferences.getString("accessToken", null)

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2101"

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        academicYearsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val academicYearlist = AcademicYear()
                            academicYearlist.setYearId(c.getString("id"))
                            academicYearlist.setAcademicYear(c.getString("defaultId"))

                            academicYearsArrayList.add(academicYearlist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_ACADEMIC_YEAR + "(academicYearID, academicYear) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    c.getString("id"),
                                    c.getString("defaultId")
                                )
                            )

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerCalendarData", e.toString())
                    }

                }, Response.ErrorListener { e -> Log.d("loadServerCalendarData", e.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }


    private fun requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}
