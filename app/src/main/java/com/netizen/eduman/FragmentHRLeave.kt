package com.netizen.eduman

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.HRPresentAdapter
import com.netizen.eduman.FragmentHRLeaveAtdSelection.Companion.hr_attendance_leave_date
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.HRPresent
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class FragmentHRLeave : Fragment(), View.OnClickListener {
    private var btn_serach_hr_present: Button? = null

    internal var hrPresentsArrayList = ArrayList<HRPresent>()
    private var recyclerView: RecyclerView? = null
    private var adapter: HRPresentAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var et_p_hr_id_search: EditText? = null

   // private var hr_attendance_Leave_date: String? = null
    lateinit var v: View
    private var hr_attndance_smry_date: EditText? = null
    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null

    private var txt_back: TextView? = null


    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_attendance_recyler_list_view, container, false)

        initializeViews()
        recyclerViewInit()
        GetTotalHRAttendanceLeaveSummary()

        return v
    }

    private fun initializeViews() {
        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)
        activity?.title = Html.fromHtml("<font color='#2F3292'>HR LEAVE</font>")


        et_p_hr_id_search = v.findViewById<View>(R.id.et_p_hr_id_search) as EditText



/*        hr_attndance_smry_date = v.findViewById<View>(R.id.hr_attendance_summary_date) as EditText
        dateFormatter = SimpleDateFormat("MM/dd/yyyy", Locale.US)
        hr_attndance_smry_date?.inputType = InputType.TYPE_NULL
        hr_attndance_smry_date?.setOnClickListener(this)

        btn_serach_hr_present = v.findViewById<View>(R.id.btn_hr_attendance_search) as Button

        btn_serach_hr_present?.setOnClickListener {
            try {
                val strUserName = hr_attndance_smry_date?.getText().toString()

                if (TextUtils.isEmpty(strUserName)) {
                    hr_attndance_smry_date?.error = "Please input date"
                } else {
                    GetTotalHRAttendanceLeaveSummary()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }*/


      //  setDateTimeField()

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = TabFragmentHPAL()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        et_p_hr_id_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    override fun onClick(v: View) {
        if (v === hr_attndance_smry_date) {
            fromDatePickerDialog?.show()
        }


    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                hr_attndance_smry_date?.setText(dateFormatter?.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        // fromDatePickerDialog?.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", fromDatePickerDialog)

    }

    fun recyclerViewInit() {
        // Initialize item list
        hrPresentsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.hr_attdnce_smry_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = HRPresentAdapter(activity!!, hrPresentsArrayList)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView!!.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView!!.adapter = adapter
    }

    fun GetTotalHRAttendanceLeaveSummary() {
        //  val hitURL = "http://192.168.31.5:8082/staff/attendance/inout/time/details?attendanceDate=02/23/2019&inOutStatus=Leave&access_token="
      //  hr_attendance_Leave_date = hr_attndance_smry_date?.text.toString() //send to server


        val hitURL =
            AppController.BaseUrl + "staff/attendance/inout/time/details?attendanceDate=" + hr_attendance_leave_date + "&inOutStatus=Leave&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data....") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val hrPresent = HRPresent()

                        hrPresent.setHr_p_id(c.getString("customStaffId"))
                        hrPresent.setHr_p_name(c.getString("staffName"))
                        hrPresent.setHr_p_category(c.getString("staffCategory"))
                        hrPresent.setHr_p_designation(c.getString("staffDesignation"))
                        hrPresent.setHr_p_mobile(c.getString("staffMobileNo"))
                        // hrPresent.setHr_p_in_time(c.getString("stringInTime"));
                        // adding item to ITEM list
                        hrPresentsArrayList.add(hrPresent)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }
                if (hrPresentsArrayList.isEmpty()) {
                    AppController.instance?.Alert(activity!!, R.drawable.not_found, "Failure", "Sorry no data found.")
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }

    companion object {
        private val TAG = "FragmentHRLeave"
    }
}
