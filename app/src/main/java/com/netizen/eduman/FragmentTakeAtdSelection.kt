package com.netizen.eduman


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.Html
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Period
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.text.SimpleDateFormat
import java.util.*

class FragmentTakeAtdSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_get_atd_list: Button? = null
    private var btn_take_student_atd: Button? = null
    private var btn_student_atd_list: Button? = null
    private var btn_hr_atd_list: Button? = null

    private var spinner_section: Spinner? = null
    private var spinner_period: Spinner? = null

    private var inputAttendanceDate: EditText? = null

    private var selectSection: String? = null
    private var selectPeriod: String? = null
    //private var localPeriodID: String? = null
    //private var dateOfAttendance: String? = null
    //private var localSectionID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var periodsArrayList = ArrayList<Period>()

    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.take_student_atd_selection, container, false)
        initializeViews()
        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_take_atd_selection")
        editor.apply()

        return v
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onStart() {
        super.onStart()
        setsectionSpinnerData()
       // loadServerSectionData()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {
        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        activity?.title = Html.fromHtml("<font color='#2F3292'>ATTENDANCE</font>")

        btn_get_atd_list = v?.findViewById<View>(R.id.btn_get_atd_list) as Button

        btn_take_student_atd = v?.findViewById<View>(R.id.btn_take_student_atd) as Button
        btn_student_atd_list = v?.findViewById<View>(R.id.btn_student_atd_list) as Button
        btn_hr_atd_list = v?.findViewById<View>(R.id.btn_hr_atd_list) as Button

        inputAttendanceDate = v?.findViewById<View>(R.id.attendance_of_date) as EditText

        spinner_section = v?.findViewById<View>(R.id.spinner_attendance_sec) as Spinner
        spinner_section?.onItemSelectedListener = this

        spinner_period = v?.findViewById<View>(R.id.spinner_attendance_period) as Spinner
        spinner_period?.onItemSelectedListener = this

        setDateTimeField()

        dateFormatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        inputAttendanceDate?.inputType = InputType.TYPE_NULL
        inputAttendanceDate?.setOnClickListener(this)

        btn_get_atd_list?.setOnClickListener {
            try {

                val senrollmentFragment = FragmentTakeAttendance()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_student_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentSAttendanceSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_hr_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = TabFragmentHPAL()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun onClick(v: View) {
        if (v === inputAttendanceDate)
            fromDatePickerDialog!!.show()
    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                inputAttendanceDate!!.setText(dateFormatter!!.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)
        )
    }


    private fun loadServerSectionData() {

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)

                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_SECTION + "(classConfigId, classShiftSection) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("classConfigId"), c.getString("classShiftSection"))
                            )

                            if (!sectionsArrayList.isEmpty()) {
                              //  setsectionSpinnerData(sectionsArrayList)
                            }

                        }

                        activity?.let { AppController.instance?.hideProgress(it) }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                        activity?.let { AppController.instance?.hideProgress(it) }

                    }
                },
                Response.ErrorListener { e ->
                    Log.d("Attendance : Section", e.toString())
                    activity?.let { AppController.instance?.hideProgress(it) }

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerPeriodData() {
        val hitURL =
            AppController.mainUrl + "manual/attendance/period/list?access_token=" + accessToken + "&attendanceDate=$dateOfAttendance&classConfigId=$localSectionID"

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        periodsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val periodlist = Period()
                            periodlist.setPeriodId(c.getString("periodId"))
                            periodlist.setPeriodName(c.getString("periodName"))

                            periodsArrayList.add(periodlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_ATTENDANCE + "(periodId, periodName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("periodId"), c.getString("periodName"))
                            )

                            if (!periodsArrayList.isEmpty()) {
                                setperiodSpinnerData(periodsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in attendances?.indices!!) {
                attendances.get(i).getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance  ", " Section Spinner data")
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setperiodSpinnerData(periodsArrayList: ArrayList<Period>) {
        try {
            val period = ArrayList<String>()
            period.add(resources.getString(R.string.select_period))
            for (i in periodsArrayList.indices) {
                period.add(periodsArrayList[i].getPeriodName().toString())
            }

            val periodAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, period)
            periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_period?.adapter = periodAdapter
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance  ", " Period Spinner data")
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            /*if (parent.getItemAtPosition(position).equals("Select Section")) {

            }*/
            if (spinner.id == R.id.spinner_attendance_sec) {
                selectSection = spinner_section!!.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    selectSection = parent.getItemAtPosition(position).toString()
                    val da = activity?.let { DAO(it) }
                    da?.open()
                    localSectionID = da?.GetSectionID(selectSection!!).toString()
                    dateOfAttendance = inputAttendanceDate?.text.toString()

                    da?.deleteTakeAttendance()

                    loadServerPeriodData()

                    da?.close()

                }
            }


            if (spinner.id == R.id.spinner_attendance_period) {
                selectPeriod = spinner_period!!.selectedItem.toString()
                if (selectPeriod == resources.getString(R.string.select_period)) {
                    selectPeriod = ""
                } else {
                    selectPeriod = parent.getItemAtPosition(position).toString()

                    val da = activity?.let { DAO(it) }
                    da?.open()
                    localPeriodID = da?.GetPeriodID(selectPeriod!!).toString()

                    GetAllStudentAttendanceWebService()
                    da?.close()

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Attendance take", "Selected Spinner data")
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }


    @SuppressLint("SimpleDateFormat")
    fun GetAllStudentAttendanceWebService() {

        val mainFormat = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        val parsedDate = mainFormat.parse(dateOfAttendance)
        val formatGet = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val date = formatGet.format(parsedDate)

        val hitURL =
            AppController.mainUrl + "student/list/by/class-config-id?access_token=" + accessToken + "&attendanceDate=$date&classConfigId=$localSectionID&periodId=$localPeriodID"

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }


        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                VolleyLog.d(FragmentTakeAtdSelection.TAG, "Server Response:$response")
                try {
                    val getData = response.getJSONArray("item")
                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + DBHelper.TABLE_TAKEATTENDANCE + "(studentId, studentRoll, studentName, studentGender, identificationId) " +
                                    "VALUES(?, ?, ?, ?, ?)"),
                            arrayOf(
                                c.getString("studentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("studentGender"),
                                c.getString("identificationId")
                            )
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }

/*                if (attendancessArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(activity!!, R.drawable.not_found, "Failure", "Sorry no data found.")
                }*/

                //Notify adapter if data change
                // adapter!!.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                // total_found?.text = dao?.totalAbssentStudent().toString()
                dao?.close()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                VolleyLog.d(FragmentTakeAtdSelection.TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }

    companion object {
        private val TAG = "FragmentTakeAtdSelection"
        var localSectionID = ""
        var localPeriodID = ""
        var dateOfAttendance = ""
    }

}
