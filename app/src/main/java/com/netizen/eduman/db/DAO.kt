package com.netizen.eduman.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DBHelper.Companion.TABLE_ACADEMIC_YEAR
import com.netizen.eduman.db.DBHelper.Companion.TABLE_LOGIN
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_INPUT_STUDENT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_SCALE_DISTRIBUTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_STUDENT_RESULT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_TAKEATTENDANCE
import com.netizen.eduman.model.*
import java.util.*


/*
 * Data access object class
 * Data Access Objects are the main classes where we define our database interactions
 */
class DAO(context: Context) {

    // Database fields
    private var db: SQLiteDatabase? = null
    private val dbHelper: DBHelper

    init {
        dbHelper = DBHelper(context)
    }

    @Throws(Throwable::class)
    protected fun finalize() {
        // TODO Auto-generated method stub
        if (db != null && db!!.isOpen)
            db!!.close()
    }

    /*
    Open any close database object
     */
    @Throws(SQLException::class)
    fun open() {
        //Create and/or open a database that will be used for reading and writing.
        db = dbHelper.writableDatabase
    }

    /*
     Close any open database object.
     */
    fun close() {
        dbHelper.close()
    }

    /*
    select query for retrieving data from table
    returns a set of rows and columns in a Cursor.
     */
    fun getRecordsCursor(sql: String, param: Array<String>): Cursor? {
        var curs: Cursor? = null
        curs = db?.rawQuery(sql, param)
        return curs
    }


    /*
    execSQL doesn't return anything and used for creating,updating, replacing
     */
    @Throws(SQLException::class)
    fun execSQL(sql: String, param: Array<String>) {
        db!!.execSQL(sql, param)
    }


    companion object {
        private val TAG = DAO::class.java.simpleName

        /*
        executeSQL doesn't return anything and used for creating,updating, replacing
        Not need to create extra DAO object .
     */
        fun executeSQL(sql: String, param: Array<String>) {
            val da = AppController.instance?.let { DAO(it) }
            da?.open()
            try {
                da?.execSQL(sql, param)
            } catch (e: Exception) {
                throw e
            } finally {
                da?.close()
            }
        }
    }

    // TODO Auto-generated catch block
    val allTakeAttendance: ArrayList<Attendance>
        get() {
            val attendancesArrayList = ArrayList<Attendance>()
            var attendance: Attendance?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_TAKEATTENDANCE ORDER BY identificationId DESC",
                    arrayOf(
                        "[studentId]",
                        "[studentRoll]",
                        "[studentName]",
                        "[studentGender]",
                        "[identificationId]",
                        "[checked]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        attendance = Attendance(
                            curs.getString(0),
                            curs.getString(1),
                            curs.getString(2),
                            curs.getString(3),
                            curs.getString(4),
                            curs.getInt(5)

                        )
                        attendancesArrayList.add(attendance)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return attendancesArrayList
        }


    fun GetSectionID(sectionShiftName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [classConfigId] FROM Section WHERE classShiftSection = '$sectionShiftName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs?.moveToFirst()!!) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetPeriodID(periodName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [periodId] FROM Attendance WHERE periodName = '$periodName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun UpdateSingleCategoryFlag(cflag: Int, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[checked]", cflag)
        db!!.update(
            TABLE_TAKEATTENDANCE, cv, "[identificationId]=?",
            arrayOf(identificationId)
        )

    }

    fun totalAbssentStudent(): Int {
        var nombr = 0
        val cursor = db!!.rawQuery("SELECT studentId FROM TakeAttendance", null)
        nombr = cursor.count
        return nombr
    }

    fun GetAtSummaryPeriodID(periodName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [typeId] FROM AttendanceSummaryPeriod WHERE periodName = '$periodName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getDefaultIdFromMarkDT(): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [defaultId] FROM MarkScaleDistribution"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetExamCode2(de: String): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [shortCodeName] FROM MarkScaleDistribution WHERE defaultId = $de"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetExamCode3(): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [shortCodeName] FROM MarkScaleDistribution WHERE defaultId = 3"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun UpdateExamCode2(examCode2: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode2]", examCode2)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode3(examCode3: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode3]", examCode3)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }


    //May 2-2019

    fun getALLInputMark(): ArrayList<InputMark> {
        val inputMarkArrayList = ArrayList<InputMark>()
        var inputMark: InputMark? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                "$TABLE_MARK_INPUT_STUDENT ORDER BY identificationId DESC",
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentName]",
                    "[studentRoll]",
                    "[shortCode1]",
                    "[shortCode2]",
                    "[shortCode3]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    inputMark = InputMark(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6)
                    )
                    inputMarkArrayList.add(inputMark)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return inputMarkArrayList
    }

    fun getALLMarkDistrubutionScale(): ArrayList<MarkDistribution> {
        val attendancesArrayList = ArrayList<MarkDistribution>()
        var attendance: MarkDistribution? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_MARK_SCALE_DISTRIBUTION,
                arrayOf("[shortCodeName]", "[passMark]", "[totalMark]", "[acceptance]", "[defaultId]"),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    attendance = MarkDistribution(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4)

                    )
                    attendancesArrayList.add(attendance)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return attendancesArrayList
    }


    fun UpdateRemarkStudentFlag(cflag: Int, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[checked]", cflag)
        db?.update(
            TABLE_STUDENT_RESULT, cv, "[identificationId]=?",
            arrayOf(identificationId)
        )
    }

    fun getAllRemarksStudent(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_STUDENT_RESULT + " ORDER BY identificationId DESC",
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[remarkId]",
                    "[remarks]",
                    "[remarksTitle]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getString(7),
                        curs.getString(8),
                        curs.getString(9),
                        curs.getInt(10)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun getAllRemarksStudentUpdate(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_STUDENT_RESULT + " WHERE checked = 1",
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[remarkId]",
                    "[remarks]",
                    "[remarksTitle]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getString(7),
                        curs.getString(8),
                        curs.getString(9),
                        curs.getInt(10)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun UpdateRemarkDescription(remarksDes: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[remarks]", remarksDes)
        db?.update(
            TABLE_STUDENT_RESULT, cv, "[identificationId]=?",
            arrayOf(identificationId)
        )

    }


    fun totalRemarksUpdateStudent(): Int {
        var nombr = 0
        val cursor = db?.rawQuery("SELECT identificationId FROM StudentResult", null)
        nombr = cursor?.count!!
        return nombr
    }

    fun GetExamMark(identification: String): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [shortCode2] FROM MarkInputStudent WHERE identificationId = $identification"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    //------------------------5/9/2019-------------
    fun GetExamID(examName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [ExamId] FROM Exam WHERE ExamName = '$examName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetGroupID(grpName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [GroupId] FROM SGroup WHERE GroupName = '$grpName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetDesignationID(desigName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [DesigId] FROM HRDesignation WHERE DesigName = '$desigName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetCategoryID(categoryName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [CategoryId] FROM STCategory WHERE CategoryName = '$categoryName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetRemarkID(remarktitle: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [remarkTempId] FROM RemarksTitle WHERE remarkTitle = '$remarktitle'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    //------------------------5/16/2019---------------
    fun GetSubjectID(subName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [SubjectId] FROM InputMarkSubject WHERE SubjectName = '$subName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    //exam result summary showing .................
    fun GetExamResultID(examName: String, secPos: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [ExamId] FROM ExamResult WHERE ExamName = '$examName' AND SectionPosition ='$secPos'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun deleteUsers() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(TABLE_LOGIN, null, null)

            Log.d(TAG, "Deleted all user info from sqlite")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

/*    fun getUser(): UserLogin? {
        var ul: UserLogin? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_LOGIN,
                arrayOf(
                    "userName",
                    "password"
                ),
                null,
                null,
                null,
                null,
                null
            )

            curs!!.moveToFirst()
            if (!curs.isAfterLast) {
                ul = UserLogin(
                    curs.getString(0), curs.getString(1)
                )
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }
        return ul
    }*/


    fun getUserDetails(): UserLogin {

        //  val selectQuery = "SELECT *FROM Login where user_id= (SELECT max(user_id) FROM Login order by user_id desc limit 1)"
        try {


            val selectQuery = "SELECT *FROM Login"

            val cursor = db?.rawQuery(selectQuery, null)
            val user = UserLogin()
            // looping through all rows and adding to list
            if (cursor?.moveToFirst()!!) {
                do {
                    //user.setuserId(cursor.getString(1))
                    user.setuserName(cursor.getString(0))
                    user.setuserPassword(cursor.getString(1))

                } while (cursor.moveToNext())
            }

            return user
        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }


    fun getTakeStudentAttendanceCount(): Long {
        val count = DatabaseUtils.queryNumEntries(db, TABLE_TAKEATTENDANCE)
        db?.close()
        return count
    }


    fun deleteTakeAttendance() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(TABLE_TAKEATTENDANCE, null, null)

            Log.d(TAG, "Deleted all take attendance info from sqlite")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    // TODO Auto-generated catch block
    val allSectionData: ArrayList<Section>
        get() {
            val sectionArrayList = ArrayList<Section>()
            var section: Section?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_SECTION ORDER BY classConfigId ASC",
                    arrayOf(
                        "[classShiftSection]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        section = Section(
                            curs.getString(0)
                        )
                        sectionArrayList.add(section)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return sectionArrayList
        }


    // TODO Auto-generated catch block
    val allAcademicData: ArrayList<AcademicYear>
        get() {
            val academicYearArrayList = ArrayList<AcademicYear>()
            var academic: AcademicYear?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_ACADEMIC_YEAR ORDER BY academicYear DESC",
                    arrayOf(
                        "[academicYear]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        academic = AcademicYear(
                            curs.getString(0)
                        )
                        academicYearArrayList.add(academic)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return academicYearArrayList
        }

    fun totalAttendanceStudentCount(): Int {
        var nombr = 0
        val cursor = db?.rawQuery("SELECT studentId FROM TakeAttendance", null)
        nombr = cursor?.count!!
        return nombr
    }
}

