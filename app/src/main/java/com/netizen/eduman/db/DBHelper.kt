package com.netizen.eduman.db


import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

/*
 * Sqlite helper class to manage database creation and version management.
 */
class DBHelper(internal var context: Context)// Store the context for later use
    : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    /**
     * Only run when the database file did not exist and was just created.
     * Once we've created the database on first launch of our application,
     * we can perform any operation SQL offers, including arithmetics.
     * As for where to put the .sql file
     *
     * @param db SQLiteDatabase: The database.
     */
    override fun onCreate(db: SQLiteDatabase) {
        // TODO Auto-generated method stub
        try {
            executeSQLScript(db, "create.sql")
        } catch (e: SQLException) {
        } catch (e: IOException) {
        }
    }

    /**
     * onUpgrade() is only called when the database file exists
     * but the stored version number is lower than requested in constructor.
     * The onUpgrade() should update the table schema to the requested version.
     *
     * @param db         SQLiteDatabase: The database.
     * @param oldVersion int: The old database version.
     * @param newVersion int: The new database version.
     */
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        try {
            if (newVersion > oldVersion) {
                when (oldVersion) {
                    1 -> executeSQLScript(db, "update_v2.sql")
                }
            }
        } catch (e: SQLException) {
        } catch (e: IOException) {
        }

    }

    /**
     * @param database SQLiteDatabase: The database.
     * @param dbname   Database name
     * @throws IOException  Constructs an IOException with null as its error detail message.
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    @Throws(IOException::class, SQLException::class)
    private fun executeSQLScript(database: SQLiteDatabase, dbname: String) {
        //Creates a new byte array output stream
        val outputStream = ByteArrayOutputStream()
        //Creates a newly allocated byte array.
        val buf = ByteArray(1024)
        var len: Int
        //Provides access to an application's raw asset files to retrieve their resource data
        val assetManager = context.assets
        var inputStream: InputStream? = null

        try {
            // Used for reading
            inputStream = assetManager.open(dbname)

/*            while ((len = inputStream!!.read(buf)) != -1) {
                outputStream.write(buf, 0, len)
            }*/

            while (inputStream.read(buf).let { len = it; it != -1 }) {
                outputStream.write(buf, 0, len)
            }

            outputStream.close()
            inputStream!!.close()

            val createScript =
                outputStream.toString().split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in createScript.indices) {
                val sqlStatement = createScript[i].trim { it <= ' ' }
                // TODO You may want to parse out comments here
                if (sqlStatement.length > 0) {
                    try {
                        database.execSQL("$sqlStatement;")
                    } catch (se: SQLException) {
                        Log.e(TAG, se.toString(), se)
                    }

                }
            }
        } catch (e: IOException) {
            // TODO Handle Script Failed to Load
            Log.e(TAG, e.toString(), e)
            throw e
        } catch (e: SQLException) {
            // TODO Handle Script Failed to Execute
            Log.e(TAG, e.toString(), e)
            throw e
        }

    }

    companion object {
        private val TAG = SQLiteOpenHelper::class.java.simpleName

        //Db version
        internal val DB_VERSION = 1
        // Db version name
        internal val DB_NAME = "eduman5.ndb"

        // Db table
        val TABLE_ACADEMIC_YEAR = "AcademicYear"
        val TABLE_SECTION = "Section"
        val TABLE_GROUP = "SGroup"
        val TABLE_HR_DESIG = "HRDesignation"
        val TABLE_ST_CATEGORY = "STCategory"
        val TABLE_ST_REMARKS_TITLE = "RemarksTitle"
        val TABLE_INPUT_MARK_SUB = "InputMarkSubject"


        val TABLE_EXAM = "Exam"
        val TABLE_EXAM_RESULT = "ExamResult"
        val TABLE_LOGIN = "Login"
        val TABLE_TOKEN = "Token"

        val TABLE_ATTENDANCE = "Attendance"
        val TABLE_TAKEATTENDANCE = "TakeAttendance"
        val TABLE_ATTENDANCESUMMARYPERIOD = "AttendanceSummaryPeriod"
        val TABLE_MARK_SCALE_DISTRIBUTION = "MarkScaleDistribution"
        val TABLE_MARK_INPUT_STUDENT = "MarkInputStudent"
        val TABLE_STUDENT_RESULT = "StudentResult"

    }

}
