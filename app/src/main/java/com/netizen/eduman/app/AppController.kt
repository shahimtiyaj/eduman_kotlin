package com.netizen.eduman.app

import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDelegate
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class AppController : Application() {
    private var mRequestQueue: RequestQueue? = null

    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }
            return this.mRequestQueue!!
        }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    companion object {

        val TAG = AppController::class.java.simpleName
        val BaseUrl = "https://api.netizendev.com:2083/emapi/"
        //val BaseUrl = "http://192.168.31.14:8080/"
        val EndUrl = "functions/items"
        val AccessTokenCreation = ""
        private var progressDialog: ProgressDialog? = null

        var context: Context? = null
            private set
        @get:Synchronized
        var instance: AppController? = null
            private set

        val mainUrl: String
            get() = BaseUrl

    }

    fun Alert(ctx: Context, icon: Int, title: String, message: String) {
        AlertDialog.Builder(ctx)
            .setIcon(icon)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which -> dialog.dismiss() }
            .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }.show()
    }

    fun showProgress(context: Context, title: String, message: String) {
        progressDialog = ProgressDialog(context)
        progressDialog?.setTitle(title)
        progressDialog?.setMessage(message)
        progressDialog?.show()
    }

    fun hideProgress(context: Context) {
        progressDialog?.hide()
    }
}
