package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_GROUP
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_ST_CATEGORY
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Category
import com.netizen.eduman.model.Group
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.util.ArrayList
import java.util.HashMap

class FragmentSEnrollmentForm : Fragment(), AdapterView.OnItemSelectedListener {
    private var btn_reg: Button? = null
    private var btn_enroll_student: Button? = null
    private var btn_find_student: Button? = null

    private var txt_back: TextView? = null
    private var spinner_group: Spinner? = null
    private var spinner_category: Spinner? = null
    private var spinner_session: Spinner? = null
    private var spinner_section: Spinner? = null
    private var spinner_student_id: Spinner? = null
    private var spinner_gender: Spinner? = null
    private var spinner_religion: Spinner? = null

    private var inputStudentCustomID: EditText? = null
    private var inputStudentRoll: EditText? = null
    private var inputStudentName: EditText? = null
    private var inputFatherName: EditText? = null
    private var inputMotherName: EditText? = null
    private var inputPhoneNo: EditText? = null
    private var custom_id_head: TextView? = null

    private var studentCustom: String? = null
    private var rollNo: String? = null
    private var name: String? = null
    private var fname: String? = null
    private var mname: String? = null
    private var phone: String? = null
    private var selectAcademic: String? = null
    private var selectSection: String? = null
    private var selectGroup: String? = null
    private var selectCategory: String? = null
    private var selectstdIdType: String? = null
    private var selectGender: String? = null
    private var selectReligion: String? = null
    private var pos: String? = null
    private val check: String? = null
    private var localSectionID: String? = null
    private var localGroupID: String? = null
    private var localCategoryID: String? = null

    private var std_id_type: String? = null
    private val inputLayoutCustomID: TextInputLayout? = null
    private var inputLayoutRoll: TextInputLayout? = null
    private var inputLayoutName: TextInputLayout? = null
    private var inputLayoutFaName: TextInputLayout? = null
    private var inputLayoutMaName: TextInputLayout? = null
    private var inputLayouMobile: TextInputLayout? = null

    private val toolbar: Toolbar? = null
    private val toolbarTitle: TextView? = null

    internal var academicYearsArrayList = ArrayList<AcademicYear>()
    internal var categoriesArrayList = ArrayList<Category>()
    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()

    private var jObjPost: String? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var v: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_enrollment_laypout, container, false)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_reg")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()
        setAcademicYearSpinnerData()
        setsectionSpinnerData()
    }

    private fun initializeViews() {
        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)
        //activity?.title = "STUDENT"
        activity?.setTitle(Html.fromHtml("<font color='#2F3292'>STUDENT</font>"))

        inputStudentCustomID = v!!.findViewById<View>(R.id.et_student_custom_id) as EditText
        inputStudentRoll = v?.findViewById<View>(R.id.et_student_roll_no) as EditText
        inputStudentName = v?.findViewById<View>(R.id.et_student_name) as EditText
        inputFatherName = v?.findViewById<View>(R.id.et_student_father_name) as EditText
        inputMotherName = v?.findViewById<View>(R.id.et_student_mother_name) as EditText
        inputPhoneNo = v?.findViewById<View>(R.id.et_student_mobile) as EditText
        custom_id_head = v?.findViewById<View>(R.id.srudent_head) as TextView

        inputLayoutRoll = v?.findViewById<View>(R.id.input_layout_roll) as TextInputLayout
        inputLayoutName = v?.findViewById<View>(R.id.input_layout_name) as TextInputLayout
        inputLayoutFaName = v?.findViewById<View>(R.id.input_layout_father_name) as TextInputLayout
        inputLayoutMaName = v?.findViewById<View>(R.id.input_layout_mother_name) as TextInputLayout
        inputLayouMobile = v?.findViewById<View>(R.id.input_layout_mobile) as TextInputLayout

        inputStudentCustomID?.addTextChangedListener(MyTextWatcher(inputStudentCustomID!!))
        inputStudentRoll?.addTextChangedListener(MyTextWatcher(inputStudentRoll!!))
        inputStudentName?.addTextChangedListener(MyTextWatcher(inputStudentName!!))
        inputFatherName?.addTextChangedListener(MyTextWatcher(inputFatherName!!))
        inputMotherName?.addTextChangedListener(MyTextWatcher(inputMotherName!!))
        inputPhoneNo?.addTextChangedListener(MyTextWatcher(inputPhoneNo!!))

        btn_reg = v?.findViewById<View>(R.id.btn_reg_submit) as Button
        btn_enroll_student = v?.findViewById<View>(R.id.btn_enroll_student) as Button
        btn_find_student = v?.findViewById<View>(R.id.btn_find_student) as Button


        // txt_back = v!!.findViewById<View>(R.id.back_from_std_reg_page) as TextView

        spinner_session = v?.findViewById<View>(R.id.spinner_session) as Spinner
        spinner_session?.onItemSelectedListener = this

        spinner_section = v?.findViewById<View>(R.id.spinner_section) as Spinner
        spinner_section?.onItemSelectedListener = this

        spinner_group = v?.findViewById<View>(R.id.spinner_group) as Spinner
        spinner_group?.onItemSelectedListener = this

        spinner_category = v?.findViewById<View>(R.id.spinner_category) as Spinner
        spinner_category?.onItemSelectedListener = this

        spinner_student_id = v?.findViewById<View>(R.id.spinner_std_id_type) as Spinner
        spinner_student_id?.onItemSelectedListener = this

        spinner_gender = v?.findViewById<View>(R.id.spinner_gender) as Spinner
        spinner_gender?.onItemSelectedListener = this

        spinner_religion = v?.findViewById<View>(R.id.spinner_religion) as Spinner
        spinner_religion?.onItemSelectedListener = this

/*        txt_back!!.setOnClickListener {
            try {
                val `in` = Intent(activity, MainActivity::class.java)
                startActivity(`in`)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }*/

        btn_reg?.setOnClickListener {
            try {
                submitRegistration()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

/*        btn_enroll_student?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentSEnrollmentForm()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }*/


        btn_find_student?.setOnClickListener {
            try {
                val allstdListFragment = FragGetStudentListSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setAcademicYearSpinnerData() {
        try {


            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val academicYears = dao?.allAcademicData

            val academicYear = ArrayList<String>()
            academicYear.add(resources.getString(R.string.select_session))
            for (i in academicYears?.indices!!) {
                academicYears[i].getAcademicYear()?.let { academicYear.add(it) }
            }

            val academicAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, academicYear)
            academicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_session?.adapter = academicAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }

    }

    private fun setcategorySpinnerData(categoryArrayList: ArrayList<Category>) {
        try {
            val category = ArrayList<String>()
            category.add(resources.getString(R.string.select_category))
            for (i in categoryArrayList.indices) {
                categoryArrayList[i].getCategoryName()?.let { category.add(it) }
            }

            val categoryAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, category)
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_category?.adapter = categoryAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in attendances?.indices!!) {
                attendances.get(i).getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    private fun setgroupSpinnerData(groupArrayList: ArrayList<Group>) {
        try {
            val group = ArrayList<String>()
            group.add(resources.getString(R.string.select_group))
            for (i in groupArrayList.indices) {
                groupArrayList[i].getGroupName()?.let { group.add(it) }
            }

            val groupAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, group)
            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_group?.adapter = groupAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_session) {

                selectAcademic = spinner_session?.selectedItem.toString()
                if (selectAcademic == resources.getString(R.string.select_session)) {
                    selectAcademic = ""
                } else {
                    pos = position.toString()
                    selectAcademic = parent.getItemAtPosition(position).toString()
                    //loadServerSectionData()
                }
            }

            if (spinner.id == R.id.spinner_section) {
                selectSection = spinner_section?.selectedItem.toString()
                if (selectSection == resources.getString(R.string.select_section)) {
                    selectSection = ""
                } else {
                    pos = position.toString()
                    selectSection = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localSectionID = da.GetSectionID(selectSection!!)
                    loadServerGroupData()
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_group) {
                selectGroup = spinner_group?.selectedItem.toString()
                if (selectGroup == resources.getString(R.string.select_group)) {
                    selectGroup = ""
                } else {
                    pos = position.toString()
                    selectGroup = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localGroupID = da.GetGroupID(selectGroup!!)
                    loadServerCategoryData()
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_category) {
                selectCategory = spinner_category?.selectedItem.toString()
                if (selectCategory == resources.getString(R.string.select_category)) {
                    selectCategory = ""
                } else {
                    pos = position.toString()
                    selectCategory = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localCategoryID = da.GetCategoryID(selectCategory!!)
                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_gender) {
                selectGender = spinner_gender?.selectedItem.toString()
                if (selectGender == resources.getString(R.string.select_gender)) {
                    selectGender = ""
                } else {
                    pos = position.toString()
                    selectGender = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_religion) {
                selectReligion = spinner_religion!!.selectedItem.toString()
                if (selectReligion == resources.getString(R.string.select_gender)) {
                    selectReligion = ""
                } else {
                    pos = position.toString()
                    selectReligion = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_std_id_type) {
                selectstdIdType = spinner_student_id?.selectedItem.toString()
                if (selectstdIdType == resources.getString(R.string.select_student_id_type)) {
                    selectstdIdType = ""
                } else {
                    pos = position.toString()
                    selectstdIdType = parent.getItemAtPosition(position).toString()
                    if (selectstdIdType == "Custom") {
                        std_id_type = 1.toString()
                        inputStudentCustomID?.visibility = View.VISIBLE
                        custom_id_head?.visibility = View.VISIBLE

                    } else {
                        std_id_type = 0.toString()
                        inputStudentCustomID?.visibility = View.GONE
                        custom_id_head?.visibility = View.GONE
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private fun submitRegistration() {
        if (!validateStudentAcademicYear()) {
            return
        }

        if (!validateStudentSection()) {
            return
        }

        if (!validateStudentGroup()) {
            return
        }
        if (!validateStudentCategory()) {
            return
        }

        /*if (!validateStudentID()) {
            return
        }*/

        if (!validateStudentRoll()) {
            return
        }
        if (!validateName()) {
            return
        }

        if (!validateStudentFatherName()) {
            return
        }
        if (!validateStudentMotherName()) {
            return
        }

        if (!validateStudentGender()) {
            return
        }

        if (!validateStudentReligion()) {
            return
        }

        if (!validatePhone()) {
            return
        }
        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        rollNo = inputStudentRoll!!.text.toString()
        name = inputStudentName!!.text.toString()
        fname = inputFatherName!!.text.toString()
        mname = inputMotherName!!.text.toString()
        phone = inputPhoneNo!!.text.toString()
        studentCustom = inputStudentCustomID!!.text.toString()
        dataSendToServer()
    }

    private fun validatePhone(): Boolean {
        if (inputPhoneNo!!.text.toString().trim { it <= ' ' }.isEmpty() || inputPhoneNo!!.length() != 11) {
            inputLayouMobile!!.error = getString(R.string.err_msg_phone)
            requestFocus(inputPhoneNo!!)
            return false
        } else {
            inputLayouMobile!!.isErrorEnabled = false
        }
        return true
    }


    private fun validateName(): Boolean {
        if (inputStudentName!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentName!!.error = getString(R.string.err_msg_name)
            requestFocus(inputStudentName!!)
            return false
        } else {
            inputLayoutName!!.isErrorEnabled = false
        }
        return true
    }

    private fun validateStudentID(): Boolean {
        if (inputStudentCustomID?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutCustomID?.setError(getString(R.string.err_mssg_st_id))
            inputStudentCustomID?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutCustomID?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateStudentRoll(): Boolean {
        if (inputStudentRoll?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentRoll?.setError(getString(R.string.err_mssg_st_roll))
            inputStudentRoll?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutRoll?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateStudentFatherName(): Boolean {
        if (inputFatherName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputFatherName?.setError(getString(R.string.err_mssg_fname))
            inputFatherName?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutFaName?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateStudentMotherName(): Boolean {
        if (inputMotherName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputMotherName?.setError(getString(R.string.err_mssg_mname))
            inputMotherName?.let { requestFocus(it) }
            return false
        } else {
            inputLayoutMaName?.setErrorEnabled(false)
        }
        return true
    }

    private fun validateStudentAcademicYear(): Boolean {
        if (selectAcademic?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Academic Year", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateStudentSection(): Boolean {
        if (selectSection?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Section", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateStudentGroup(): Boolean {
        if (selectGroup?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Group", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateStudentCategory(): Boolean {
        if (selectCategory?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Category", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateStudentGender(): Boolean {
        if (selectGender?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Gender", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun validateStudentReligion(): Boolean {
        if (selectReligion?.trim { it <= ' ' }!!.isEmpty()) {
            Toast.makeText(activity, "Select Religion", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et_student_name -> validateName()
                R.id.et_student_mobile -> validatePhone()
            }
        }
    }

    private fun loadServerAcademicYearData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2101"

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }


        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        academicYearsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val academicYearlist = AcademicYear()
                            academicYearlist.setYearId(c.getString("id"))
                            academicYearlist.setAcademicYear(c.getString("defaultId"))

                            academicYearsArrayList.add(academicYearlist)

                            if (!academicYearsArrayList.isEmpty()) {
                                // setAcademicYearSpinnerData(academicYearsArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerCalendarData", e.toString())
                        activity?.let { AppController.instance?.hideProgress(it) }

                    }
                    activity?.let { AppController.instance?.hideProgress(it) }

                }, Response.ErrorListener { e -> Log.d("loadServerCalendarData", e.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerCategoryData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2106"


        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        categoriesArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val categorylist = Category()
                            categorylist.setCategoryId(c.getString("id"))
                            categorylist.setCategoryName(c.getString("name"))

                            categoriesArrayList.add(categorylist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_ST_CATEGORY + "(CategoryId, CategoryName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("id"), c.getString("name"))
                            )

                            if (!categoriesArrayList.isEmpty()) {
                                setcategorySpinnerData(categoriesArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerCategoryData", e.toString())
                    }
                }, Response.ErrorListener { error ->
                    Log.d("loadServerCategoryData", error.toString())
                    Log.d("loadServerCategoryData", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    private fun loadServerSectionData() {

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_SECTION + "(classConfigId, classShiftSection, instituteId) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(
                                    c.getString("classConfigId"),
                                    c.getString("classShiftSection"),
                                    c.getString("instituteId")
                                )
                            )

                            if (!sectionsArrayList.isEmpty()) {
                                // setsectionSpinnerData(sectionsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error -> Log.d("loadServerSectionData", error.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    private fun loadServerGroupData() {
        val hitURL =
            AppController.BaseUrl + "core/setting/group-configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        groupsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val obj = c.getJSONObject("classObject")

                            val grouplist = Group()
                            grouplist.setGroupId(obj.getString("id"))
                            grouplist.setGroupName(obj.getString("name"))

                            groupsArrayList.add(grouplist)

                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_GROUP + "(GroupId, GroupName) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    obj.getString("id"),
                                    obj.getString("name")
                                )
                            )

                            if (!groupsArrayList.isEmpty()) {
                                setgroupSpinnerData(groupsArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerGroupData", e.toString())
                    }
                }, Response.ErrorListener { error -> Log.d("loadServerGroupData", error.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    fun dataSendToServer() {
        val hitURL =
            AppController.BaseUrl + "student/list/save?registrationType=" + std_id_type + "&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Wait", "Sending data to the server") }

        val jsonObj = JSONObject()
        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {
            arrayItem.put("academicSession", "2017-2018")
            arrayItem.put("bloodGroup", "O+")
            arrayItem.put("customStudentId", studentCustom)
            arrayItem.put("fatherName", fname)
            arrayItem.put("motherName", mname)
            arrayItem.put("studentGender", selectGender)
            arrayItem.put("studentName", name)
            arrayItem.put("studentReligion", selectReligion)
            arrayItem.put("studentRoll", rollNo)
            arrayItem.put("guardianMobile", phone)

            jsonArray.put(arrayItem)

            jsonObj.put("academicYear", selectAcademic)
            jsonObj.put("classConfigId", localSectionID)//117556
            jsonObj.put("groupId", localGroupID)
            jsonObj.put("studentCategoryId", localCategoryID)
            jsonObj.put("studentRequestHelpers", jsonArray)


        } catch (e: JSONException) {
            e.printStackTrace()
        }

        //here convert json object to string for posting data to server
        jObjPost = jsonObj.toString()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        Toast.makeText(activity, status, Toast.LENGTH_LONG).show()
                        activity?.let { AppController.instance?.hideProgress(it) }

                    } else {
                        Toast.makeText(activity, "Registration fail!!", Toast.LENGTH_LONG).show()
                        activity?.let { AppController.instance?.hideProgress(it) }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

            }) {

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun onBackPressed() {

        val count = activity?.supportFragmentManager?.backStackEntryCount

        if (count == 0) {
            super.getActivity()?.onBackPressed()
            //additional code
        } else {
            activity?.supportFragmentManager?.popBackStack()
        }
    }


/*    fun onBackPressed() {
        if (activity?.supportFragmentManager?.backStackEntryCount!! > 0) {
            activity?.supportFragmentManager?.popBackStackImmediate()
        } else {
            super.getActivity()?.onBackPressed()
        }
    }*/

    /* fun addFragment(fragment: Fragment, addToBackStack: Boolean, tag: String) {
         val manager = activity?.supportFragmentManager
         val ft = manager?.beginTransaction()

         if (addToBackStack) {
             ft?.addToBackStack(tag)
         }
         ft?.replace(R.id.container_frame, fragment, tag)
         ft?.commitAllowingStateLoss()
     }*/

    companion object {

        private val TAG = "FragmentSEnrollmentForm"
    }
}

