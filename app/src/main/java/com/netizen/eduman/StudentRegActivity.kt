package com.netizen.eduman

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Category
import com.netizen.eduman.model.Group
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class StudentRegActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private val Tag = javaClass.name
    private val context = this@StudentRegActivity
    private var btn_reg: Button? = null
    private var spinner_group: Spinner? = null
    private var spinner_category: Spinner? = null
    private var spinner_session: Spinner? = null
    private var spinner_section: Spinner? = null
    private var spinner_student_id: Spinner? = null
    private var spinner_gender: Spinner? = null
    private var spinner_religion: Spinner? = null

    private var inputStudentCustomID: EditText? = null
    private var inputStudentRoll: EditText? = null
    private var inputStudentName: EditText? = null
    private var inputFatherName: EditText? = null
    private var inputMotherName: EditText? = null
    private var inputPhoneNo: EditText? = null

    private var studentCustom: String? = null
    private var rollNo: String? = null
    private var name: String? = null
    private var fname: String? = null
    private var mname: String? = null
    private var phone: String? = null
    private var selectAcademic: String? = null
    private var selectSection: String? = null
    private var selectGroup: String? = null
    private var selectCategory: String? = null
    private var selectstdIdType: String? = null
    private var selectGender: String? = null
    private var selectReligion: String? = null
    private var pos: String? = null
    private val check: String? = null
    private var std_id_type: String? = null
    private val inputLayoutCustomID: TextInputLayout? = null
    private var inputLayoutRoll: TextInputLayout? = null
    private var inputLayoutName: TextInputLayout? = null
    private var inputLayoutFaName: TextInputLayout? = null
    private var inputLayoutMaName: TextInputLayout? = null
    private var inputLayouMobile: TextInputLayout? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    internal var academicYearsArrayList = ArrayList<AcademicYear>()
    internal var categoriesArrayList = ArrayList<Category>()
    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()

    private var jObjPost: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_reg_auto_id)
        // loadServerData();
        toolBarInit()
        initializeViews()
        loadServerData()
        loadServerCategoryData()
        loadServerSectionData()
        loadServerGroupData()
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById(R.id.toolbar) as Toolbar
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar!!.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbar!!.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun initializeViews() {
        inputStudentCustomID = findViewById(R.id.et_student_custom_id) as EditText

        inputStudentRoll = findViewById(R.id.et_student_roll_no) as EditText
        inputStudentName = findViewById(R.id.et_student_name) as EditText
        inputFatherName = findViewById(R.id.et_student_father_name) as EditText
        inputMotherName = findViewById(R.id.et_student_mother_name) as EditText
        inputPhoneNo = findViewById(R.id.et_student_mobile) as EditText

        inputLayoutRoll = findViewById(R.id.input_layout_roll) as TextInputLayout
        inputLayoutName = findViewById(R.id.input_layout_name) as TextInputLayout
        inputLayoutFaName = findViewById(R.id.input_layout_father_name) as TextInputLayout
        inputLayoutMaName = findViewById(R.id.input_layout_mother_name) as TextInputLayout
        inputLayouMobile = findViewById(R.id.input_layout_mobile) as TextInputLayout

        inputStudentCustomID!!.addTextChangedListener(MyTextWatcher(inputStudentCustomID!!))
        inputStudentRoll!!.addTextChangedListener(MyTextWatcher(inputStudentRoll!!))
        inputStudentName!!.addTextChangedListener(MyTextWatcher(inputStudentName!!))
        inputFatherName!!.addTextChangedListener(MyTextWatcher(inputFatherName!!))
        inputMotherName!!.addTextChangedListener(MyTextWatcher(inputMotherName!!))
        inputPhoneNo!!.addTextChangedListener(MyTextWatcher(inputPhoneNo!!))


        btn_reg = findViewById(R.id.btn_reg_submit) as Button

        spinner_session = findViewById(R.id.spinner_session) as Spinner
        spinner_session!!.onItemSelectedListener = this

        spinner_section = findViewById(R.id.spinner_section) as Spinner
        spinner_section!!.onItemSelectedListener = this

        spinner_group = findViewById(R.id.spinner_group) as Spinner
        spinner_group!!.onItemSelectedListener = this

        spinner_category = findViewById(R.id.spinner_category) as Spinner
        spinner_category!!.onItemSelectedListener = this

        spinner_student_id = findViewById(R.id.spinner_std_id_type) as Spinner
        spinner_student_id!!.onItemSelectedListener = this

        spinner_gender = findViewById(R.id.spinner_gender) as Spinner
        spinner_gender!!.onItemSelectedListener = this

        spinner_religion = findViewById(R.id.spinner_religion) as Spinner
        spinner_religion!!.onItemSelectedListener = this


        btn_reg!!.setOnClickListener {
            try {
                submitRegistration()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun loadServerData() {

        val hitURL ="https://api.netizendev.com:2083/emapi/core/setting/list/by-type-id?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc&typeId=2101"

        val jsonObjectRequest = object : JsonObjectRequest(Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                try {
                    academicYearsArrayList = ArrayList()
                    //JSONObject jsonObj = response.getJSONObject("result");
                    val getData = response.getJSONArray("item")

                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        val academicYearlist = AcademicYear()
                        academicYearlist.setYearId(c.getString("id"))
                        academicYearlist.setAcademicYear(c.getString("defaultId"))

                        academicYearsArrayList.add(academicYearlist)

                        if (!academicYearsArrayList.isEmpty()) {
                            setAcademicYearSpinnerData(academicYearsArrayList)
                        }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(this@StudentRegActivity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    this@StudentRegActivity,
                    error.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"
                headers["typeId"] = "2101"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    private fun loadServerCategoryData() {

        val hitURL ="https://api.netizendev.com:2083/emapi/core/setting/list/by-type-id?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc&typeId=2106"

        val jsonObjectRequest = object : JsonObjectRequest(Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                try {
                    categoriesArrayList = ArrayList()
                    //JSONObject jsonObj = response.getJSONObject("result");
                    val getData = response.getJSONArray("item")

                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        val categorylist = Category()
                        categorylist.setCategoryId(c.getString("id"))
                        categorylist.setCategoryName(c.getString("name"))

                        categoriesArrayList.add(categorylist)

                        if (!categoriesArrayList.isEmpty()) {
                            setcategorySpinnerData(categoriesArrayList)
                        }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(this@StudentRegActivity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    this@StudentRegActivity,
                    error.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"
                headers["typeId"] = "2106"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }


    private fun loadServerSectionData() {

        val hitURL ="https://api.netizendev.com:2083/emapi/core/setting/class-configuration/list?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc"

        val jsonObjectRequest = object : JsonObjectRequest(Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                try {
                    sectionsArrayList = ArrayList()
                    //JSONObject jsonObj = response.getJSONObject("result");
                    val getData = response.getJSONArray("item")

                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        val sectionlist = Section()
                        sectionlist.setSectionId(c.getString("classConfigId"))
                        sectionlist.setSectionName(c.getString("classShiftSection"))

                        sectionsArrayList.add(sectionlist)

                        if (!sectionsArrayList.isEmpty()) {
                            setsectionSpinnerData(sectionsArrayList)
                        }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(this@StudentRegActivity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    this@StudentRegActivity,
                    error.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }


    private fun loadServerGroupData() {

        val hitURL ="https://api.netizendev.com:2083/emapi/core/setting/group-configuration/list/by/class-config-id?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc&classConfigId=114102"


        val jsonObjectRequest = object : JsonObjectRequest(Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                try {
                    groupsArrayList = ArrayList()
                    //JSONObject jsonObj = response.getJSONObject("result");
                    val getData = response.getJSONArray("item")

                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        val obj = c.getJSONObject("classObject")

                        val grouplist = Group()
                        grouplist.setGroupId(obj.getString("id"))
                        grouplist.setGroupName(obj.getString("name"))

                        groupsArrayList.add(grouplist)

                        if (!groupsArrayList.isEmpty()) {
                            setgroupSpinnerData(groupsArrayList)
                        }

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(this@StudentRegActivity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    this@StudentRegActivity,
                    error.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"
                headers["classConfigId"] = "114102"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    fun dataSendToServer() {
        val a = "7e1bb8bc-c8ef-4821-8811-97a1eef7135c"
        val hitURL = "http://192.168.31.14:8083/student/list/save?registrationType=$selectstdIdType&access_token=$a"
        val jsonObj = JSONObject()
        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {

            arrayItem.put("academicSession", "2017-2018")
            arrayItem.put("bloodGroup", "O+")
            arrayItem.put("customStudentId", studentCustom)
            arrayItem.put("fatherName", fname)
            arrayItem.put("motherName", mname)
            arrayItem.put("studentGender", selectGender)
            arrayItem.put("studentName", name)
            arrayItem.put("studentReligion", selectReligion)
            arrayItem.put("studentRoll", rollNo)
            arrayItem.put("guardianMobile", phone)


            jsonArray.put(arrayItem)

            jsonObj.put("academicYear", selectAcademic)
            jsonObj.put("classConfigId", "117556")
            jsonObj.put("groupId", selectGroup)
            jsonObj.put("studentCategoryId", selectCategory)

            jsonObj.put("studentRequestHelpers", jsonArray)


        } catch (e: JSONException) {
            e.printStackTrace()
        }

        //here convert json object to string for posting data to server
        jObjPost = jsonObj.toString()

        val postRequest = object : CustomRequest(Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        Toast.makeText(context, status, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, "Registration fail!!", Toast.LENGTH_LONG).show()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        applicationContext,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(
                        applicationContext,
                        "Parsing error! Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        applicationContext,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"
                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }


    private fun setAcademicYearSpinnerData(academicYearsArrayList: ArrayList<AcademicYear>) {
        val academicYear = ArrayList<String>()
        academicYear.add(resources.getString(R.string.select_session))
        for (i in academicYearsArrayList.indices) {
            academicYear.add(academicYearsArrayList[i].getAcademicYear().toString())
        }

        val academicAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, academicYear)
        academicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_session!!.adapter = academicAdapter
    }

    private fun setcategorySpinnerData(categoryArrayList: ArrayList<Category>) {
        val category = ArrayList<String>()
        category.add(resources.getString(R.string.select_category))
        for (i in categoryArrayList.indices) {
            category.add(categoryArrayList[i].getCategoryName().toString())
        }

        val categoryAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, category)
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_category!!.adapter = categoryAdapter
    }


    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>) {
        val section = ArrayList<String>()
        section.add(resources.getString(R.string.select_section))
        for (i in sectionArrayList.indices) {
            sectionArrayList[i].getSectionName()?.let { section.add(it) }
        }

        val sectionAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, section)
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_section!!.adapter = sectionAdapter
    }

    private fun setgroupSpinnerData(groupArrayList: ArrayList<Group>) {
        val group = ArrayList<String>()
        group.add(resources.getString(R.string.select_group))
        for (i in groupArrayList.indices) {
            group.add(groupArrayList[i].getGroupName().toString())
        }

        val groupAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, group)
        groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_group!!.adapter = groupAdapter
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val spinner = parent as Spinner

        if (spinner.id == R.id.spinner_session) {
            selectAcademic = spinner_session!!.selectedItem.toString()
            if (selectAcademic == resources.getString(R.string.select_session)) {
                selectAcademic = ""
            } else {
                pos = position.toString()
                selectAcademic = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_section) {
            selectSection = spinner_section!!.selectedItem.toString()
            if (selectSection == resources.getString(R.string.select_section)) {
                selectSection = ""
            } else {
                pos = position.toString()
                selectSection = parent.getItemAtPosition(position).toString()
            }
        }


        if (spinner.id == R.id.spinner_group) {
            selectGroup = spinner_group!!.selectedItem.toString()
            if (selectGroup == resources.getString(R.string.select_group)) {
                selectGroup = ""
            } else {
                pos = position.toString()
                selectGroup = parent.getItemAtPosition(position).toString()
                Toast.makeText(parent.getContext(), "Blood:$pos&Blood G:$selectAcademic", Toast.LENGTH_LONG).show()
            }
        }

        if (spinner.id == R.id.spinner_category) {
            selectCategory = spinner_category!!.selectedItem.toString()
            if (selectCategory == resources.getString(R.string.select_category)) {
                selectCategory = ""
            } else {
                pos = position.toString()
                selectCategory = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_gender) {
            selectGender = spinner_gender!!.selectedItem.toString()
            if (selectGender == resources.getString(R.string.select_gender)) {
                selectGender = ""
            } else {
                pos = position.toString()
                selectGender = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_religion) {
            selectReligion = spinner_religion!!.selectedItem.toString()
            if (selectReligion == resources.getString(R.string.select_gender)) {
                selectReligion = ""
            } else {
                pos = position.toString()
                selectReligion = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_std_id_type) {
            selectstdIdType = spinner_student_id!!.selectedItem.toString()
            if (selectstdIdType == resources.getString(R.string.select_student_id_type)) {
                selectstdIdType = ""
            } else {
                pos = position.toString()
                selectstdIdType = parent.getItemAtPosition(position).toString()
                if (selectstdIdType == "Custom") {
                    std_id_type = 1.toString()
                    inputStudentCustomID!!.visibility = View.VISIBLE

                } else {
                    std_id_type = 0.toString()
                    inputStudentCustomID!!.visibility = View.GONE
                }
            }
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private fun submitRegistration() {
        /*        if (!validateName()) {
            return;
        }
        if (!validatePhone()) {
            return;
        }*/
        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        rollNo = inputStudentRoll!!.text.toString()
        name = inputStudentName!!.text.toString()
        fname = inputFatherName!!.text.toString()
        mname = inputMotherName!!.text.toString()
        phone = inputPhoneNo!!.text.toString()
        studentCustom = inputStudentCustomID!!.text.toString()


        dataSendToServer()
    }


    /*
    find the birthday day from edit text
     */

    private fun validatePhone(): Boolean {
        if (inputPhoneNo!!.text.toString().trim { it <= ' ' }.isEmpty() || inputPhoneNo!!.length() != 11) {
            inputLayouMobile!!.error = getString(R.string.err_msg_phone)
            requestFocus(inputPhoneNo!!)
            return false
        } else {
            inputLayouMobile!!.isErrorEnabled = false
        }
        return true
    }


    private fun validateName(): Boolean {
        if (inputStudentName!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentName!!.error = getString(R.string.err_msg_name)
            requestFocus(inputStudentName!!)
            return false
        } else {
            inputLayoutName!!.isErrorEnabled = false
        }
        return true
    }


    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private inner class MyTextWatcher (private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et_student_name -> validateName()
                R.id.et_student_mobile -> validatePhone()
            }
        }
    }

    companion object {

        private val TAG = "StudentRegActivity"
    }
}
