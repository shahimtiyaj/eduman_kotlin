package com.netizen.eduman

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.Html
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Period
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import org.json.JSONException
import java.text.SimpleDateFormat
import java.util.*

class FragmentSAttendanceSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_get_atd_list: Button? = null
    private var btn_take_student_atd: Button? = null
    private var btn_student_atd_list: Button? = null
    private var btn_hr_atd_list: Button? = null

    private var spinner_section: Spinner? = null
    private var spinner_period: Spinner? = null

    private var spinner_attendance_summary_period: Spinner? = null

    private var inputAttendanceSummaryDate: EditText? = null

    private var selectPeriod: String? = null
  //  private var dateOfAttendanceSummary: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var periodsArrayList = ArrayList<Period>()

    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.attendance_summary_selection_layout, container, false)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_atd_list_selection")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()
        loadServerPeriodData()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {
        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        activity?.title = Html.fromHtml("<font color='#2F3292'>ATTENDANCE</font>")

        btn_get_atd_list = v?.findViewById<View>(R.id.btn_all_attendance_summary_search) as Button
        btn_take_student_atd = v?.findViewById<View>(R.id.btn_take_student_atd) as Button
        btn_student_atd_list = v?.findViewById<View>(R.id.btn_student_atd_list) as Button
        btn_hr_atd_list = v?.findViewById<View>(R.id.btn_hr_atd_list) as Button
        inputAttendanceSummaryDate = v?.findViewById<View>(R.id.attendance_summary_date) as EditText

        spinner_attendance_summary_period = v?.findViewById<View>(R.id.spinner_attendance_summary_period) as Spinner
        spinner_attendance_summary_period?.onItemSelectedListener = this

        setDateTimeField()

        dateFormatter = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        inputAttendanceSummaryDate?.inputType = InputType.TYPE_NULL
        inputAttendanceSummaryDate?.setOnClickListener(this)

        btn_get_atd_list?.setOnClickListener {

            try {
                try {
                    if (localPeriodID == null && localSectionID == null && inputAttendanceSummaryDate == null) {
                        getActivity()?.let { it1 ->
                            AppController.instance?.Alert(
                                it1,
                                R.drawable.requred_alert,
                                "Failure",
                                "Please Fill up the required fields."
                            )
                        }
                    } else {
                        val senrollmentFragment = FragmentAttendanceSummary()
                        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                        fragmentTransaction.commit()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_take_student_atd?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_hr_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = TabFragmentHPAL()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun onClick(v: View) {
        if (v === inputAttendanceSummaryDate)
            fromDatePickerDialog?.show()
    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                inputAttendanceSummaryDate?.setText(dateFormatter!!.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)
        )
    }


    private fun loadServerPeriodData() {

        val hitURL = AppController.mainUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2301"

        // val hitURL = "https://api.netizendev.com:2083/emapi/core/setting/list/by-type-id?access_token=63baa5dc-6f5f-4f09-a85d-bb106bfaf882&typeId=2301"
        // val hitURL = AppController.mainUrl + "manual/attendance/period/list?access_token=" + accessToken + "&attendanceDate=$dateOfAttendanceSummary"

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        periodsArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val periodlist = Period()
                            periodlist.setPeriodId(c.getString("id"))
                            periodlist.setPeriodName(c.getString("name"))

                            periodsArrayList.add(periodlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_ATTENDANCESUMMARYPERIOD + "(typeId, periodName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("id"), c.getString("name"))
                            )

                            if (!periodsArrayList.isEmpty()) {
                                setperiodSpinnerData(periodsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Log.d("Atd summary", "Period data")
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setperiodSpinnerData(periodsArrayList: ArrayList<Period>) {
        try {
            val period = ArrayList<String>()
            period.add(resources.getString(R.string.select_period))
            for (i in periodsArrayList.indices) {
                periodsArrayList[i].getPeriodName()?.let { period.add(it) }
            }

            val periodAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, period)
            periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_attendance_summary_period?.adapter = periodAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: java.lang.IllegalStateException) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: java.lang.IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Atd Summary exam ", "Period spinner data")
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_attendance_summary_period) {
                selectPeriod = spinner_attendance_summary_period!!.selectedItem.toString()
                if (selectPeriod == resources.getString(R.string.select_period)) {
                    selectPeriod = ""
                } else {
                    selectPeriod = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localPeriodID = da.GetAtSummaryPeriodID(selectPeriod!!)
                    //inputAttendanceSummaryDate?.getText()?.clear()
                    dateOfAttendanceSummary = inputAttendanceSummaryDate?.text.toString()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Atd Summary exam ", "Period spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }


    companion object {
        private val TAG = "FragmentTakeAtdSelection"
        var localSectionID = ""
        var localPeriodID = ""
        var dateOfAttendanceSummary = ""
    }
}
