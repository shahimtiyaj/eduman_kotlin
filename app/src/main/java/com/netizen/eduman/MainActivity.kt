package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFullScreenWindow()
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val dashboardFragment = FragmentDashboard()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
        fragmentTransaction.commit()

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        toggle.drawerArrowDrawable.color = resources.getColor(R.color.colorEduman)

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun showExit() {
        val builder = AlertDialog.Builder(this@MainActivity, R.style.DialogTheme)
        builder.setMessage("Are you sure you want to Logout from Eduman App?")
            .setTitle("Logout")
            .setPositiveButton("YES") { _, _ -> finish() }
            .setNegativeButton("CANCEL") { _, _ -> }
        builder.show()
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        }

        else if(!drawer.isDrawerOpen(GravityCompat.START)){
            val sharedPreferences = getSharedPreferences("back", Context.MODE_PRIVATE)

            when (sharedPreferences.getString("now", "")) {
                "std_reg" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentDashboard()).commit()
                "std_all_list" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragGetStudentListSelection()).commit()
                "std_take_atd" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentTakeAtdSelection()).commit()
                "std_take_atd_selection" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentDashboard()).commit()
                "hr_reg" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentDashboard()).commit()
                "hr_list" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentHRnrollmentForm()).commit()
                "std_atd_list_selection" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentTakeAtdSelection()).commit()
                "input_mark_selection" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "input_mark_std_details" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  TabFragmentMIU()).commit()
                "update_mark_std_details" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  TabFragmentMIU()).commit()
                "semester_exam_dashboard" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentDashboard()).commit()
                "general_exam_process" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "grand_final_exam_process" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "merit_position_process" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "student_result_selection" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "student_result_details" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentStudentResultSelection()).commit()
                "remark_assign_selection" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  FragmentSemesterExamDash()).commit()
                "remark_assign_details" -> supportFragmentManager.beginTransaction().replace(R.id.fragment_container,  TabFragmentRAU()).commit()

                "home" -> showExit()

            }

        }

       /* else {
            super.onBackPressed()
        }*/
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_home) {
            val dashboardFragment = FragmentDashboard()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
            fragmentTransaction.commit()
        } else if (id == R.id.nav_student) {
            val tabstdFragment = FragmentSEnrollmentForm()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabstdFragment)
            fragmentTransaction.commit()
        } else if (id == R.id.nav_hr_management) {
            val tabhrmFragment = FragmentHRnrollmentForm()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabhrmFragment)
            fragmentTransaction.commit()
        } else if (id == R.id.nav_all_attendance) {
            val atdtDashFragment = FragmentTakeAtdSelection()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, atdtDashFragment)
            fragmentTransaction.commit()
        } else if (id == R.id.nav_all_semester_exam) {
            val fragmentSemesterExam = FragmentSemesterExamDash()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentSemesterExam)
            fragmentTransaction.commit()
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}
