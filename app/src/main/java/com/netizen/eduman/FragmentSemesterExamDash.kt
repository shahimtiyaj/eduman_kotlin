package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class FragmentSemesterExamDash : Fragment() {

    private var markscardId: CardView? = null
    private var examProcesscardId: CardView? = null
    private var attendance_count_cardId: CardView? = null
    private var resultcardId: CardView? = null
    private var back_cardId: CardView? = null
    private var remarks_cardId: CardView? = null
    private var v: View? = null
    private var txt_back: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.semester_exam_dashboard, container, false)

        markscardId = v!!.findViewById<View>(R.id.markscardId) as CardView
        examProcesscardId = v!!.findViewById<View>(R.id.examProcesscardId) as CardView
        //attendance_count_cardId = v!!.findViewById<View>(R.id.attendance_count_cardId) as CardView
        resultcardId = v!!.findViewById<View>(R.id.resultcardId) as CardView
        remarks_cardId = v!!.findViewById<View>(R.id.remarks_cardId) as CardView

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        activity?.title = Html.fromHtml("<font color='#2F3292'>SEMESTER EXAM</font>")

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "semester_exam_dashboard")
        editor.apply()

        OnclickView()
        return v
    }

    fun OnclickView() {
        markscardId?.setOnClickListener {
            try {
                val tabFragmentMIU = TabFragmentMIU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, tabFragmentMIU)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        examProcesscardId?.setOnClickListener {
            val tabFragmentExmP = TabFragmentExmP()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabFragmentExmP)
            fragmentTransaction.commit()
        }

      /*  attendance_count_cardId!!.setOnClickListener {
            val fragmentAttdncCount = FragmentAttdncCount()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentAttdncCount)
            fragmentTransaction.commit()
        }*/

        resultcardId?.setOnClickListener {
            val fragmentStudentResult = FragmentStudentResultSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentStudentResult)
            fragmentTransaction.commit()
        }

        remarks_cardId?.setOnClickListener {
            val tabFragmentRAU = TabFragmentRAU()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabFragmentRAU)
            fragmentTransaction.commit()
        }

     /*   back_cardId!!.setOnClickListener {
            val fragmentDashboard = FragmentDashboard()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentDashboard)
            fragmentTransaction.commit()
        }*/

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}