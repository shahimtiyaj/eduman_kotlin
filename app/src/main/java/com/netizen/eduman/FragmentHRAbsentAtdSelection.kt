package com.netizen.eduman

import android.app.DatePickerDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.InputType
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

class FragmentHRAbsentAtdSelection : Fragment(), View.OnClickListener {
    private var btn_serach_hr_present: Button? = null

    private var btn_take_student_atd: Button? = null
    private var btn_student_atd_list: Button? = null

    private var hr_attndance_smry_date: EditText? = null
    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    lateinit var v: View

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_attendance_selection, container, false)
        initializeViews()
        return v

    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_take_student_atd = v.findViewById<View>(R.id.btn_take_student_atd) as Button
        btn_student_atd_list = v.findViewById<View>(R.id.btn_student_atd_list) as Button

        hr_attndance_smry_date = v.findViewById<View>(R.id.hr_attendance_summary_date) as EditText
        dateFormatter = SimpleDateFormat("MM/dd/yyyy", Locale.US)
        hr_attndance_smry_date?.inputType = InputType.TYPE_NULL
        hr_attndance_smry_date?.setOnClickListener(this)

        btn_serach_hr_present = v.findViewById<View>(R.id.btn_hr_attendance_search) as Button

        btn_serach_hr_present?.setOnClickListener {
            try {
                hr_attendance_absent_date = hr_attndance_smry_date?.getText().toString()

                if (TextUtils.isEmpty(hr_attendance_absent_date)) {
                    hr_attndance_smry_date?.error = "Please input date"
                }
                else{
                    val senrollmentFragment = FragmentHRAbsent()
                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        setDateTimeField()

        btn_take_student_atd?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_student_atd_list?.setOnClickListener {
            try {
                val senrollmentFragment = FragmentSAttendanceSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onClick(v: View) {
        if (v === hr_attndance_smry_date) {
            fromDatePickerDialog?.show()
        }

    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                hr_attndance_smry_date!!.setText(dateFormatter!!.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)
        )

    }

    companion object {

        private val TAG = "FragmentHRPresent"
        var hr_attendance_absent_date = ""

    }
}
