package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.Adapter.StudentListAdapter
import com.netizen.eduman.FragGetStudentListSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.Student
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.util.*

class FragmentStudentList : Fragment() {

    private var spinner_get_all_std_list: Spinner? = null
    private var selectSection_get_all_std_list: String? = null
    //  private var localSectionID: String? = null
    private val classConfigId: String? = null
    internal var sectionsArrayList_all_std = ArrayList<Section>()
    private var txt_back: TextView? = null
    private var et_student_id_search: EditText? = null


    private var recyclerView: RecyclerView? = null
    private var adapter: StudentListAdapter? = null
    private var studentsArrayList: ArrayList<Student>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_recyler_list, container, false)
        initializeViews()
        recyclerViewInit()

        GetAllStudentDataWebService()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_all_list")
        editor.apply()

        return v
    }

    override fun onResume() {
        super.onResume()
        // loadServerSectionData()
    }

    private fun initializeViews() {
        //  spinner_get_all_std_list = v?.findViewById<View>(R.id.select_section_get_all_std_list) as SearchableSpinner
        // spinner_get_all_std_list?.onItemSelectedListener = this

        // activity?.title = "STUDENT"
        activity?.setTitle(Html.fromHtml("<font color='#2F3292'>STUDENT</font>"))


        txt_back = v?.findViewById<View>(R.id.back_text) as TextView
        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragGetStudentListSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        et_student_id_search = v?.findViewById<View>(R.id.et_student_id_search) as EditText


        et_student_id_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                adapter?.getFilter()?.filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    fun recyclerViewInit() {
        // Initialize item list
        studentsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_student_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = activity?.let { StudentListAdapter(studentsArrayList, it) }
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>) {
        try {
            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in sectionArrayList.indices) {
                sectionArrayList[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_get_all_std_list?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Student List ", "Section spinner data")
        }
    }

/*    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            *//*          if (spinner.id == R.id.select_section_get_all_std_list) {
                          selectSection_get_all_std_list = spinner_get_all_std_list!!.selectedItem.toString()
                          if (selectSection_get_all_std_list == resources.getString(R.string.select_section)) {
                              selectSection_get_all_std_list = ""
                          } else {
                              studentsArrayList?.clear()
                              selectSection_get_all_std_list = parent.getItemAtPosition(position).toString()

                              val da = DAO(activity!!)
                              da.open()
                              localSectionID = da.GetSectionID(selectSection_get_all_std_list!!)

                              GetAllStudentDataWebService()

                              da.close()
                          }
                      }*//*
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student List ", "Section spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }*/

    fun GetAllStudentDataWebService() {
        val hitURL =
            AppController.BaseUrl + "student/list/by/class-config-id?classConfigId=" + localSectionID + "&access_token=" + accessToken

        activity?.let { AppController.instance?.showProgress(it, "Please Wait", "Loading data...") }

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val studentlist = Student()
                        //set the json data in the model
                        studentlist.setstudent_id(c.getString("studentId"))
                        studentlist.setstudent_roll(c.getString("studentRoll"))
                        studentlist.setstudent_name(c.getString("studentName"))
                        studentlist.setgroup(c.getString("groupName"))
                        studentlist.setcategory(c.getString("studentCategoryName"))
                        studentlist.setgender(c.getString("studentGender"))
                        studentlist.setreligion(c.getString("studentReligion"))
                        studentlist.setfather_name(c.getString("fatherName"))
                        studentlist.setmother_name(c.getString("motherName"))
                        studentlist.setmobile_no(c.getString("guardianMobile"))
                        studentlist.setstudent_image(c.getString("image"))

                        // adding item to ITEM list
                        studentsArrayList?.add(studentlist)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    activity?.let { AppController.instance?.hideProgress(it) }

                }

                if (studentsArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(
                        activity!!,
                        R.drawable.not_found,
                        "Failure",
                        "Sorry no data found."
                    )
                }

                //Notify adapter if data change
                adapter?.notifyDataSetChanged()
                activity?.let { AppController.instance?.hideProgress(it) }

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                activity?.let { AppController.instance?.hideProgress(it) }

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {
            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun loadServerSectionData() {

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList_all_std = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))
                            sectionsArrayList_all_std.add(sectionlist)
                            // Insert json data into local database for  showing data into offline
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_SECTION + "(classConfigId, classShiftSection, instituteId) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(
                                    c.getString("classConfigId"),
                                    c.getString("classShiftSection"),
                                    c.getString("instituteId")
                                )
                            )

                            if (!sectionsArrayList_all_std.isEmpty()) {
                                setsectionSpinnerData(sectionsArrayList_all_std)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener { error -> Log.d("Studentlist, sec", error.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        private val TAG = "AllStudentListActivity"
    }
}

